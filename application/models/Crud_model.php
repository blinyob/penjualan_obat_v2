<?php 

class Crud_model extends CI_Model
{

	function __construct(){
		parent::__construct();		
	}

	function create(){
		$this->db->insert("pj_obatku",array("nama"=>""));
		return $this->db->insert_id();
	}


	function read(){
		$this->db->order_by("kode_barang","desc");
		$query=$this->db->get("pj_obatku");
		return $query->result_array();
	}


	function update($id,$value,$modul){
		$this->db->where(array("kode_barang"=>$id));
		$this->db->update("pj_obatku",array($modul=>$value));
	}

	function delete($id){
		$this->db->where("kode_barang",$id);
		$this->db->delete("pj_obatku");
	}


}