<?php
class M_obat extends CI_Model 
{
	function __construct(){
		parent::__construct();		
	}

	function get_obat()
	{
		$this->db->select("kode_obat,nama_obat,total_stok,harga,kategori,golongan,satuan,tgl_pembuatan,tgl_kad,keterangan");
		$this->db->from("pj_obatku");
		$this->db->join("pj_kategori_obat","pj_obatku.id_kategori_obat=pj_kategori_obat.id_kategori_obat");
		$this->db->join("pj_golongan_obat","pj_obatku.id_golongan_obat=pj_golongan_obat.id_golongan_obat");
		$this->db->join("pj_satuan_obat","pj_obatku.id_satuan_obat=pj_satuan_obat.id_satuan_obat");
		//$this->db->join("rating","rating.postingrat_id_posting=posting.id_posting");
		 $this->db->order_by('kode_obat', 'DESC');
		$result=$this->db->get();
		return $result->result_array();
	}

	function fetch_data_obat($like_value = NULL, $column_order = NULL, $column_dir = NULL, $limit_start = NULL, $limit_length = NULL)
	{
		$sql = "
			SELECT 
				(@row:=@row+1) AS nomor,
				a.`kode_obat`, 
				a.`nama_obat`,
				IF(a.`total_stok` = 0, 'Kosong', a.`total_stok`) AS total_stok,
				CONCAT('Rp. ', REPLACE(FORMAT(a.`harga`, 0),',','.') ) AS harga,
				DATE_FORMAT(a.`tgl_pembuatan`, '%d %b %Y - %H:%i:%s') AS tgl_pembuatan,
				DATE_FORMAT(a.`tgl_kad`, '%d %b %Y - %H:%i:%s') AS tgl_kad,
				a.`keterangan`,
				b.`kategori`,
				c.`golongan`,
				d.`satuan`
				
						FROM 
				`pj_obatku` AS a 
				LEFT JOIN `pj_kategori_obat` AS b ON a.`id_kategori_obat` = b.`id_kategori_obat` 
				LEFT JOIN `pj_golongan_obat` AS c ON a.`id_golongan_obat` = c.`id_golongan_obat` 
				LEFT JOIN `pj_satuan_obat` AS d ON a.`id_satuan_obat` = d.`id_satuan_obat` 
				
				, (SELECT @row := 0) r WHERE 1=1 
		";
		
		$data['totalData'] = $this->db->query($sql)->num_rows();
		
		if( ! empty($like_value))
		{
			$sql .= " AND ( ";    
			$sql .= "
				a.`kode_obat` LIKE '%".$this->db->escape_like_str($like_value)."%' 
				OR a.`nama_obat` LIKE '%".$this->db->escape_like_str($like_value)."%'
				OR IF(a.`total_stok` = 0, 'Kosong', a.`total_stok`) LIKE '%".$this->db->escape_like_str($like_value)."%' 
				OR CONCAT('Rp. ', REPLACE(FORMAT(a.`harga`, 0),',','.') ) LIKE '%".$this->db->escape_like_str($like_value)."%' 
				OR a.`keterangan` LIKE '%".$this->db->escape_like_str($like_value)."%' 
				OR DATE_FORMAT(a.`tgl_pembuatan`, '%d %b %Y - %H:%i:%s') LIKE '%".$this->db->escape_like_str($like_value)."%' 
				OR DATE_FORMAT(a.`tgl_kad`, '%d %b %Y') LIKE '%".$this->db->escape_like_str($like_value)."%' 
		
				OR b.`kategori` LIKE '%".$this->db->escape_like_str($like_value)."%'
				OR c.`golongan` LIKE '%".$this->db->escape_like_str($like_value)."%'
				OR d.`satuan` LIKE '%".$this->db->escape_like_str($like_value)."%'
				 
			";
			$sql .= " ) ";
		}
		
		$data['totalFiltered']	= $this->db->query($sql)->num_rows();
		
		$columns_order_by = array( 
			0 => 'nomor',
			1 => 'a.`kode_obat`',
			2 => 'a.`nama_obat`',
			3 => 'b.`kategori`',
			4 => 'c.`golongan`',
			5 => 'd.`satuan`',
			6 => 'a.`total_stok`',
			7 => '`harga`',
			8 => '`tgl_pembuatan`',	
			9 => '`tgl_kad`'	
		);
		
		$sql .= " ORDER BY ".$columns_order_by[$column_order]." ".$column_dir.", nomor ";
		$sql .= " LIMIT ".$limit_start." ,".$limit_length." ";
		
		$data['query'] = $this->db->query($sql);
		return $data;
	}

	function hapus_obat($id_obat)
	{
		return $this->db
				->where('kode_obat', $id_obat)
				->delete('pj_obatku');
	}

	function tambah_baru($nama, $id_kategori_obat, $stok, $harga, $tgl_pembuatan, $tgl_kad, $keterangan)
	{
		$dt = array(
			'nama_obat' => $nama,
			'total_stok' => $stok,
			'harga' => $harga,
			'id_kategori_obat' => $id_kategori_obat,
			'id_golongan_obat' => $id_golongan_obat,
			'id_satuan_obat' => $id_satuan_obat,
			'tgl_pembuatan' => $tgl_pembuatan,
			'tgl_kad' => $tgl_kad,
			'keterangan' => $keterangan
		);

		return $this->db->insert('pj_obatku', $dt);
	}

	function cek_kode($kode)
	{
		return $this->db
			->select('kode_obat')
			->where('kode_obat', $kode)
			->limit(1)
			->get('pj_obatku');
	}

	function get_baris($id_obat)
	{
		return $this->db
			->select('kode_obat, nama_obat, size, total_stok, harga, id_kategori_obat, id_golongan_obat, id_satuan_obat, tgl_pembuatan, tgl_kad, keterangan')
			->where('kode_obat', $id_obat)
			->limit(1)
			->get('pj_obatku');
	}

	function update_obat3($id,$data){
		$this->db->where('kode_obat', $id);
		$this->db->update('pj_obatku', $data);
	}

	function update_obat2($id,$value,$modul){
		$this->db->where(array("kode_obat"=>$id));
		$this->db->update("pj_obatku",array($modul=>$value));
	}

	function update_obat($id_obat, $nama, $id_kategori_obat, $id_golongan_obat, $id_satuan_obat, $stok, $harga, $tgl_pembuatan, $tgl_kad, $keterangan)
	{
		$dt = array(
			'kode_obat' => $id_obat,
			'nama_obat' => $nama,
			'total_stok' => $stok,
			'harga' => $harga,
			'id_kategori_obat' => $id_kategori_obat,
			'id_golongan_obat' => $id_golongan_obat,
			'id_satuan_obat' => $id_satuan_obat,
			
			'keterangan' => $keterangan
		);

		return $this->db
			->where('kode_obat', $id_obat)
			->update('pj_obatku', $dt);
	}

	function cari_kode($keyword, $registered)
	{
		$not_in = '';

		$koma = explode(',', $registered);
		if(count($koma) > 1)
		{
			$not_in .= " AND a.`kode_obat` NOT IN (";
			foreach($koma as $k)
			{
				$not_in .= " '".$k."', ";
			}
			$not_in = rtrim(trim($not_in), ',');
			$not_in = $not_in.")";
		}
		if(count($koma) == 1)
		{
			$not_in .= " AND a.`kode_obat` != '".$registered."' ";
		}

		$sql = "
			SELECT 
				a.`kode_obat`, a.`nama_obat`,b.`kategori`, a.`harga`,a.`total_stok` 
			FROM 
				`pj_obatku`AS a 
				LEFT JOIN `pj_kategori_obat` AS b ON a.`id_kategori_obat` = b.`id_kategori_obat`
			WHERE 
				a.`total_stok` > 0 
				AND ( 
					a.`kode_obat` LIKE '%".$this->db->escape_like_str($keyword)."%' 
					OR a.`nama_obat` LIKE '%".$this->db->escape_like_str($keyword)."%' 
				) 
				".$not_in." 
		";

		return $this->db->query($sql);
	}

	function get_stok($kode)
	{
		return $this->db
			->select('nama_obat, total_stok')
			->where('kode_obat', $kode)
			->limit(1)
			->get('pj_obatku');
	}

	function get_id($kode_obat)
	{
		return $this->db
			->select('kode_obat, nama_obat')
			->where('kode_obat', $kode_obat)
			->limit(1)
			->get('pj_obatku');
	}

	function update_stok($id_obat, $jumlah_beli)
	{
		$sql = "
			UPDATE `pj_obatku` SET `total_stok` = `total_stok` - ".$jumlah_beli." WHERE `kode_obat` = '".$id_obat."'
		";

		return $this->db->query($sql);
	}
}