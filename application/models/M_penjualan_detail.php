<?php
class M_penjualan_detail extends CI_Model
{
	function insert_detail($id_master, $id_obat, $jumlah_beli, $harga_satuan, $sub_total)
	{
		$dt = array(
			'id_penjualan_m' => $id_master,
			'kode_obat	' => $id_obat,
			'jumlah_beli' => $jumlah_beli,
			'harga_satuan' => $harga_satuan,
			'total' => $sub_total
		);

		return $this->db->insert('pj_penjualan_detail', $dt);
	}

	function get_detail($id_penjualan)
	{
		$sql = "
			SELECT 
				b.`kode_obat`,  
				b.`nama_obat`, 
				c.`kategori`, 
				CONCAT('Rp. ', REPLACE(FORMAT(a.`harga_satuan`, 0),',','.') ) AS harga_satuan, 
				a.`harga_satuan` AS harga_satuan_asli, 
				a.`jumlah_beli`,
				CONCAT('Rp. ', REPLACE(FORMAT(a.`total`, 0),',','.') ) AS sub_total,
				a.`total` AS sub_total_asli  
			FROM 
				`pj_penjualan_detail` a 
				LEFT JOIN `pj_obatku` b ON a.`kode_obat` = b.`kode_obat`
				LEFT JOIN `pj_kategori_obat` c ON b.`id_kategori_obat` = c.`id_kategori_obat` 
			WHERE 
				a.`id_penjualan_m` = '".$id_penjualan."' 
			ORDER BY 
				a.`id_penjualan_d` ASC 
		";

		return $this->db->query($sql);
	}
}