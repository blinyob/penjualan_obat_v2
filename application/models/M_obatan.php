<?php
class M_obatan extends CI_Model 
{
	function fetch_data_obatan($like_value = NULL, $column_order = NULL, $column_dir = NULL, $limit_start = NULL, $limit_length = NULL)
	{
		$sql = "
			SELECT 
				(@row:=@row+1) AS nomor, 
				a.`kode_obatan`, 
				a.`nama_obatan`,
				IF(a.`total_stok` = 0, 'Kosong', a.`total_stok`) AS total_stok,
				CONCAT('Rp. ', REPLACE(FORMAT(a.`harga`, 0),',','.') ) AS harga,
				DATE_FORMAT(a.`tgl_pembuatan`, '%d %b %Y - %H:%i:%s') AS tgl_pembuatan,
				DATE_FORMAT(a.`tgl_kadaluarsa`, '%d %b %Y') AS tgl_kadaluarsa,
				a.`keterangan`,
				b.`kategori`,
				c.`golongan`
						FROM 
				`pj_obatan` AS a 
				LEFT JOIN `pj_kategori_obatan` AS b ON a.`id_kategori_obatan` = b.`id_kategori_obatan` 
				LEFT JOIN `pj_golongan_obatan` AS c ON a.`id_golongan_obatan` = c.`id_golongan_obatan` 
				, (SELECT @row := 0) r WHERE 1=1 
		";
		
		$data['totalData'] = $this->db->query($sql)->num_rows();
		
		if( ! empty($like_value))
		{
			$sql .= " AND ( ";    
			$sql .= "
				a.`kode_obatan` LIKE '%".$this->db->escape_like_str($like_value)."%' 
				a.`nama_obatan` LIKE '%".$this->db->escape_like_str($like_value)."%'
				OR IF(a.`total_stok` = 0, 'Kosong', a.`total_stok`) LIKE '%".$this->db->escape_like_str($like_value)."%' 
				OR CONCAT('Rp. ', REPLACE(FORMAT(a.`harga`, 0),',','.') ) LIKE '%".$this->db->escape_like_str($like_value)."%' 
				OR DATE_FORMAT(a.`tgl_pembuatan`, '%d %b %Y - %H:%i:%s') LIKE '%".$this->db->escape_like_str($like_value)."%' 
				OR DATE_FORMAT(a.`tgl_kadaluarsa`, '%d %b %Y') LIKE '%".$this->db->escape_like_str($like_value)."%' 	
				OR a.`keterangan` LIKE '%".$this->db->escape_like_str($like_value)."%' 
				OR b.`kategori` LIKE '%".$this->db->escape_like_str($like_value)."%' 
				OR c.`golongan` LIKE '%".$this->db->escape_like_str($like_value)."%' 
			
				";
			$sql .= " ) ";
		}
		
		$data['totalFiltered']	= $this->db->query($sql)->num_rows();
		
		$columns_order_by = array( 
			0 => 'nomor',
			1 => 'a.`kode_obat`',
			2 => 'a.`nama_obat`',
			3 => 'b.`kategori`',
			4 => 'c.`golongan`',
			5 => 'a.`total_stok`',
			6 => '`harga`',
			7 => 'a.`tgl_pembuatan`',
			8 => 'a.`tgl_kadaluarsa`',
		);
		
		$sql .= " ORDER BY ".$columns_order_by[$column_order]." ".$column_dir.", nomor ";
		$sql .= " LIMIT ".$limit_start." ,".$limit_length." ";
		
		$data['query'] = $this->db->query($sql);
		return $data;
	}

	function hapus_obatan($id_obatan)
	{
		
		return $this->db
				->where('kode_obatan', $id_obatan)
				->delete('pj_obatan');
	}

	function tambah_baru($nama, $id_kategori_obatan,$id_golongan_obatan, $stok, $tanggal_pembuatan, $tanggal_kadaluarsa, $harga, $keterangan)
	{
		$dt = array(
			'nama_obatan' => $nama,
			'total_stok' => $stok,
			'harga' => $harga,
			'id_kategori_obatan' => $id_kategori_obatan,
			'id_golongan_obatan' => $id_golongan_obatan,
			'tgl_pembuatan' => $tanggal_pembuatan,
			'tgl_kadaluarsa' => $tanggal_kadaluarsa,
			'keterangan' => $keterangan
		);

		return $this->db->insert('pj_obatan', $dt);
	}

	function cek_kode($kode)
	{
		return $this->db
			->select('kode_obatan')
			->where('kode_obatan', $kode)
			->limit(1)
			->get('pj_obatan');
	}

	function get_baris($kode_obatan)
	{
		return $this->db
			->select('kode_obatan, nama_obatan, total_stok, harga, id_kategori_obatan, id_golongan_obatan, tgl_pembuatan, tgl_kadaluarsa, keterangan')
			->where('kode_obatan', $kode_obatan)
			->limit(1)
			->get('pj_obatan');
	}

	function update_obatan($kode_obatan, $nama, $id_kategori_obatan, $stok, $harga, $keterangan)
	{
		$dt = array(
			'nama_obatan' => $nama,
			'total_stok' => $stok,
			'harga' => $harga,
			'id_kategori_obatan' => $id_kategori_obatan,
			'keterangan' => $keterangan
		);

		return $this->db
			->where('kode_obatan', $kode_obatan)
			->update('pj_obatan', $dt);
	}

	function cari_kode($keyword, $registered)
	{
		$not_in = '';

		$koma = explode(',', $registered);
		if(count($koma) > 1)
		{
			$not_in .= " AND a.`kode_obatan` NOT IN (";
			foreach($koma as $k)
			{
				$not_in .= " '".$k."', ";
			}
			$not_in = rtrim(trim($not_in), ',');
			$not_in = $not_in.")";
		}
		if(count($koma) == 1)
		{
			$not_in .= " AND a.`kode_obatan` != '".$registered."' ";
		}

		$sql = "
			SELECT 
				a.`kode_obatan`, a.`nama_obatan`,b.`kategori`, a.`harga` 
			FROM 
				`pj_obatan`AS a 
				LEFT JOIN `pj_kategori_obatan` AS b ON a.`id_kategori_obatan` = b.`id_kategori_obatan`
			WHERE 
				a.`total_stok` > 0 
				AND ( 
					a.`kode_obatan` LIKE '%".$this->db->escape_like_str($keyword)."%' 
					OR a.`nama_obatan` LIKE '%".$this->db->escape_like_str($keyword)."%' 
				) 
				".$not_in." 
		";

		return $this->db->query($sql);
	}

	function get_stok($kode)
	{
		return $this->db
			->select('nama_obatan, total_stok')
			->where('kode_obatan', $kode)
			->limit(1)
			->get('pj_obatan');
	}

	function get_id($kode_obatan)
	{
		return $this->db
			->select('kode_obatan, nama_obatan')
			->where('kode_obatan', $kode_obatan)
			->limit(1)
			->get('pj_obatan');
	}

	function update_stok($kode_obatan, $jumlah_beli)
	{
		$sql = "
			UPDATE `pj_obatan` SET `total_stok` = `total_stok` - ".$jumlah_beli." WHERE `kode_obatan` = '".$kode_obatan."'
		";

		return $this->db->query($sql);
	}
}