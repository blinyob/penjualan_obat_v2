<?php
class M_satuan_obat extends CI_Model 
{
	function get_sat()
	{
		return $this->db
			->select('id_satuan_obat, satuan')
			->order_by('satuan', 'asc')
			->get('pj_satuan_obat');
	}

	function fetch_data_satuan($like_value = NULL, $column_order = NULL, $column_dir = NULL, $limit_start = NULL, $limit_length = NULL)
	{
		$sql = "
			SELECT 
				(@row:=@row+1) AS nomor, 
				id_satuan_obat, 
				satuan  
			FROM 
				`pj_satuan_obat`, (SELECT @row := 0) r WHERE 1=1 
		";
		
		$data['totalData'] = $this->db->query($sql)->num_rows();
		
		if( ! empty($like_value))
		{
			$sql .= " AND ( ";    
			$sql .= "
				satuan LIKE '%".$this->db->escape_like_str($like_value)."%' 
			";
			$sql .= " ) ";
		}
		
		$data['totalFiltered']	= $this->db->query($sql)->num_rows();
		
		$columns_order_by = array( 
			0 => 'nomor',
			1 => 'satuan'
		);
		
		$sql .= " ORDER BY ".$columns_order_by[$column_order]." ".$column_dir.", nomor ";
		$sql .= " LIMIT ".$limit_start." ,".$limit_length." ";
		
		$data['query'] = $this->db->query($sql);
		return $data;
	}

	function tambah_satuan($satuan)
	{
		$dt = array(
			'satuan' => $satuan
			);

		return $this->db->insert('pj_satuan_obat', $dt);
	}

	function hapus_satuan($id_satuan_obat)
	{
		return $this->db
			->where('id_satuan_obat', $id_satuan_obat)
			->delete('pj_satuan_obat');
	}

	function get_baris($id_satuan_obat)
	{
		return $this->db
			->select('id_satuan_obat, satuan')
			->where('id_satuan_obat', $id_satuan_obat)
			->limit(1)
			->get('pj_satuan_obat');
	}

	function update_satuan($id_satuan_obat, $satuan)
	{
		$dt = array(
			'satuan' => $satuan
		);

		return $this->db
			->where('id_satuan_obat', $id_satuan_obat)
			->update('pj_satuan_obat', $dt);
	}
}