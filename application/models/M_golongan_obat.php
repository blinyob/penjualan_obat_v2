<?php
class M_golongan_obat extends CI_Model 
{
	function get_gol()
	{
		return $this->db
			->select('id_golongan_obat, golongan')
			->order_by('golongan', 'asc')
			->get('pj_golongan_obat');
	}

	function fetch_data_golongan($like_value = NULL, $column_order = NULL, $column_dir = NULL, $limit_start = NULL, $limit_length = NULL)
	{
		$sql = "
			SELECT 
				(@row:=@row+1) AS nomor, 
				id_golongan_obat, 
				golongan 
			FROM 
				`pj_golongan_obat`, (SELECT @row := 0) r WHERE 1=1 
		";
		
		$data['totalData'] = $this->db->query($sql)->num_rows();
		
		if( ! empty($like_value))
		{
			$sql .= " AND ( ";    
			$sql .= "
				golongan LIKE '%".$this->db->escape_like_str($like_value)."%' 
			";
			$sql .= " ) ";
		}
		
		$data['totalFiltered']	= $this->db->query($sql)->num_rows();
		
		$columns_order_by = array( 
			0 => 'nomor',
			1 => 'golongan'
		);
		
		$sql .= " ORDER BY ".$columns_order_by[$column_order]." ".$column_dir.", nomor ";
		$sql .= " LIMIT ".$limit_start." ,".$limit_length." ";
		
		$data['query'] = $this->db->query($sql);
		return $data;
	}

	function tambah_golongan($golongan)
	{
		$dt = array(
			'golongan' => $golongan,
		);

		return $this->db->insert('pj_golongan_obat', $dt);
	}

	function hapus_golongan($id_golongan_obat)
	{
	
		return $this->db
			->where('id_golongan_obat', $id_golongan_obat)
			->delete('pj_golongan_obat');
	}

	function get_baris($id_golongan_obat)
	{
		return $this->db
			->select('id_golongan_obat, golongan')
			->where('id_golongan_obat', $id_golongan_obat)
			->limit(1)
			->get('pj_golongan_obat');
	}

	function update_golongan($id_golongan_obat, $golongan)
	{
		$dt = array(
			'golongan' => $golongan
		);

		return $this->db
			->where('id_golongan_obat', $id_golongan_obat)
			->update('pj_golongan_obat', $dt);
	}
}