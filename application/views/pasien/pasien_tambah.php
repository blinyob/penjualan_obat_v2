<?php echo form_open('pasien/tambah-pasien', array('id' => 'FormTambahpasien')); ?>
<div class='form-group'>
	<label>Nama</label>
	<input type='text' name='nama' class='form-control'>
</div>
<div class='form-group'>
	<label>Alamat</label>
	<textarea name='alamat' class='form-control' style='resize:vertical;'></textarea>
</div>
<div class='form-group'>
	<label>Nomor Telepon / Handphone</label>
	<input type='text' name='telepon' class='form-control'>
</div>
<div class='form-group'>
	<label>Info Tambahan Lainnya</label>
	<textarea name='info' class='form-control' style='resize:vertical;'></textarea>
</div>
<?php echo form_close(); ?>

<div id='ResponseInput'></div>

<script>
function Tambahpasien()
{
	$.ajax({
		url: $('#FormTambahpasien').attr('action'),
		type: "POST",
		cache: false,
		data: $('#FormTambahpasien').serialize(),
		dataType:'json',
		success: function(json){
			if(json.status == 1)
			{ 
				$('#FormTambahpasien').each(function(){
					this.reset();
				});

				if(document.getElementById('pasienArea') != null)
				{
					$('#ResponseInput').html('');

					$('.modal-dialog').removeClass('modal-lg');
					$('.modal-dialog').addClass('modal-sm');
					$('#ModalHeader').html('Berhasil');
					$('#ModalContent').html(json.pesan);
					$('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Okay</button>");
					$('#ModalGue').modal('show');

					$('#id_pasien').append("<option value='"+json.id_pasien+"' selected>"+json.nama+"</option>");
					$('#telp_pasien').html(json.telepon);
					$('#alamat_pasien').html(json.alamat);
					$('#info_tambahan_pasien').html(json.info);
				}
				else
				{
					$('#ResponseInput').html(json.pesan);
					setTimeout(function(){ 
				   		$('#ResponseInput').html('');
				    }, 3000);
					$('#my-grid').DataTable().ajax.reload( null, false );
				}
			}
			else 
			{
				$('#ResponseInput').html(json.pesan);
			}
		}
	});
}

$(document).ready(function(){
	var Tombol = "<button type='button' class='btn btn-primary' id='SimpanTambahpasien'>Simpan Data</button>";
	Tombol += "<button type='button' class='btn btn-default' data-dismiss='modal'>Tutup</button>";
	$('#ModalFooter').html(Tombol);

	$("#FormTambahpasien").find('input[type=text],textarea,select').filter(':visible:first').focus();

	$('#SimpanTambahpasien').click(function(e){
		e.preventDefault();
		Tambahpasien();
	});

	$('#FormTambahpasien').submit(function(e){
		e.preventDefault();
		Tambahpasien();
	});
});
</script>