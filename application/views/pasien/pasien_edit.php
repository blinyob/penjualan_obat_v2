<?php echo form_open('pasien/pasien-edit/'.$pasien->id_pasien, array('id' => 'FormEditpasien')); ?>

<div class='form-group'>
	<label>Nama</label>
	<?php
	echo form_input(array(
		'name' => 'nama', 
		'class' => 'form-control',
		'value' => $pasien->nama
	));
	?>
</div>
<div class='form-group'>
	<label>Alamat</label>
	<?php
	echo form_textarea(array(
		'name' => 'alamat', 
		'class' => 'form-control',
		'value' => $pasien->alamat,
		'style' => "resize:vertical",
		'rows' => 3
	));
	?>
</div>
<div class='form-group'>
	<label>Nomor Telepon / Handphone</label>
	<?php
	echo form_input(array(
		'name' => 'telepon', 
		'class' => 'form-control',
		'value' => $pasien->telp
	));
	?>
</div>
<div class='form-group'>
	<label>Info Tambahan Lainnya</label>
	<?php
	echo form_textarea(array(
		'name' => 'info', 
		'class' => 'form-control',
		'value' => $pasien->info_tambahan,
		'style' => "resize:vertical",
		'rows' => 3
	));
	?>
</div>

<?php echo form_close(); ?>

<div id='ResponseInput'></div>

<script>
function Editpasien()
{
	$.ajax({
		url: $('#FormEditpasien').attr('action'),
		type: "POST",
		cache: false,
		data: $('#FormEditpasien').serialize(),
		dataType:'json',
		success: function(json){
			if(json.status == 1){ 
				$('#ResponseInput').html(json.pesan);
				setTimeout(function(){ 
			   		$('#ResponseInput').html('');
			    }, 3000);
				$('#my-grid').DataTable().ajax.reload( null, false );
			}
			else {
				$('#ResponseInput').html(json.pesan);
			}
		}
	});
}

$(document).ready(function(){
	var Tombol = "<button type='button' class='btn btn-primary' id='SimpanEditpasien'>Update Data</button>";
	Tombol += "<button type='button' class='btn btn-default' data-dismiss='modal'>Tutup</button>";
	$('#ModalFooter').html(Tombol);

	$("#FormEditpasien").find('input[type=text],textarea,select').filter(':visible:first').focus();

	$('#SimpanEditpasien').click(function(e){
		e.preventDefault();
		Editpasien();
	});

	$('#FormEditpasien').submit(function(e){
		e.preventDefault();
		Editpasien();
	});
});
</script>