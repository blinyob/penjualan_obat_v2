<?php echo form_open('obat/tambah', array('id' => 'FormTambahobat')); ?>
<table class='table table-bordered' id='TabelTambahobat'>
	<thead>
		<tr>
			<th>#</th>
							<th>Nama obat</th>
							<th>Kategori</th>
							<th>Batch</th>
							<th>Satuan</th>
							<th>Stok</th>
							<th>Harga</th>
							<th>Tanggal Pembuatan</th>
							<th>Tanggal Kadaluarsa</th>
			<th>Keterangan</th>
			<th>Batal</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>
<?php echo form_close(); ?>

<button id='BarisBaru' class='btn btn-default'>Baris Baru</button>
<div id='ResponseInput'></div>
<link rel="stylesheet" type="text/css" href="<?php echo config_item('plugin'); ?>datetimepicker/jquery.datetimepicker.css"/>
<script src="<?php echo config_item('plugin'); ?>datetimepicker/jquery.datetimepicker.js"></script>

<script>
$(document).ready(function(){
	var Tombol = "<button type='button' class='btn btn-primary' id='SimpanTambahobat'>Simpan Data</button>";
	Tombol += "<button type='button' class='btn btn-default' data-dismiss='modal'>Tutup</button>";
	$('#ModalFooter').html(Tombol);

	BarisBaru();

	$('#BarisBaru').click(function(){
		BarisBaru();
	});

	$('#SimpanTambahobat').click(function(e){
		e.preventDefault();

		if($(this).hasClass('disabled'))
		{
			return false;
		}
		else
		{
			if($('#FormTambahobat').serialize() !== '')
			{
				$.ajax({
					url: $('#FormTambahobat').attr('action'),
					type: "POST",
					cache: false,
					data: $('#FormTambahobat').serialize(),
					dataType:'json',
					beforeSend:function(){
						$('#SimpanTambahobat').html("Menyimpan Data, harap tunggu ...");
					},
					success: function(json){
						if(json.status == 1){ 
							$('.modal-dialog').removeClass('modal-lg');
							$('.modal-dialog').addClass('modal-sm');
							$('#ModalHeader').html('Sukses !');
							$('#ModalContent').html(json.pesan);
							$('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal'>Ok</button>");
							$('#ModalGue').modal('show');
							$('#my-grid').DataTable().ajax.reload( null, false );
						}
						else {
							$('#ResponseInput').html(json.pesan);
						}

						$('#SimpanTambahobat').html('Simpan Data');
					}
				});
			}
			else
			{
				$('#ResponseInput').html('');
			}
		}
	});

	$("#FormTambahobat").find('input[type=text],textarea,select').filter(':visible:first').focus();
});

$(document).on('click', '#HapusBaris', function(e){
	e.preventDefault();
	$(this).parent().parent().remove();

	var Nomor = 1;
	$('#TabelTambahobat tbody tr').each(function(){
		$(this).find('td:nth-child(1)').html(Nomor);
		Nomor++;
	});

	$('#SimpanTambahobat').removeClass('disabled');
});

function BarisBaru()
{
	var Nomor = $('#TabelTambahobat tbody tr').length + 1;
	var Baris = "<tr>";
	Baris += "<td>"+Nomor+"</td>";
	Baris += "<td><input type='text' name='nama[]' class='form-control input-sm'></td>";
	Baris += "<td>";
	Baris += "<select name='id_kategori_obat[]' class='form-control input-sm' style='width:100px;'>";
	Baris += "<option value=''></option>";

	<?php 
	if($kategori->num_rows() > 0)
	{
		foreach($kategori->result() as $k) { ?>
			Baris += "<option value='<?php echo $k->id_kategori_obat; ?>'><?php echo $k->kategori; ?></option>";
		<?php }
	}
	?>

	Baris += "</select>";
	Baris += "</td>";
	Baris += "<td>";
	Baris += "<select name='id_golongan_obat[]' class='form-control input-sm' style='width:100px;'>";
	Baris += "<option value=''></option>";

	<?php 
	if($golongan->num_rows() > 0)
	{
		foreach($golongan->result() as $g) { ?>
			Baris += "<option value='<?php echo $g->id_golongan_obat; ?>'><?php echo $g->golongan; ?></option>";
		<?php }
	}
	?>

	Baris += "</select>";
	Baris += "</td>";
	Baris += "<td>";
	Baris += "<select name='id_satuan_obat[]' class='form-control input-sm' style='width:100px;'>";
	Baris += "<option value=''></option>";

	<?php 
	if($satuan->num_rows() > 0)
	{
		foreach($satuan->result() as $s) { ?>
			Baris += "<option value='<?php echo $s->id_satuan_obat; ?>'><?php echo $s->satuan; ?></option>";
		<?php }
	}
	?>

	Baris += "</select>";
	Baris += "</td>";

	Baris += "<td><input type='text' name='stok[]' class='form-control input-sm' onkeypress='return check_int(event)'></td>";
	Baris += "<td><input type='text' name='harga[]' class='form-control input-sm' onkeypress='return check_int(event)'></td>";
	Baris += "<td><input type='text' name='tgl_pembuatan[]' class='form-control input-sm tgl_pembuatan'></td>";
	Baris += "<td><input type='text' name='tgl_kad[]' class='form-control input-sm tgl_kad'></td>";
	
	Baris += "<td><textarea name='keterangan[]' class='form-control input-sm'></textarea></td>";
	Baris += "<td align='center'><a href='#' id='HapusBaris'><i class='fa fa-times' style='color:red;'></i></a></td>";
	Baris += "</tr>";

	$('#TabelTambahobat tbody').append(Baris);
}
$('.tgl_pembuatan').datetimepicker({
	lang:'en',
	timepicker:false,
	format:'Y-m-d',
	closeOnDateSelect:true
});
$('.tgl_kad').datetimepicker({
	lang:'en',
	timepicker:false,
	format:'Y-m-d',
	closeOnDateSelect:true
});
function check_int(evt) {
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	return ( charCode >= 48 && charCode <= 57 || charCode == 8 );
}
</script>