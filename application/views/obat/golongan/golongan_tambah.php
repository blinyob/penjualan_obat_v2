<?php echo form_open('obat/tambah-golongan', array('id' => 'FormTambahgolongan')); ?>
<div class='form-group'>
	<input type='text' name='golongan' class='form-control'>
</div>
<?php echo form_close(); ?>

<div id='ResponseInput'></div>

<script>
function Tambahgolongan()
{
	$.ajax({
		url: $('#FormTambahgolongan').attr('action'),
		type: "POST",
		cache: false,
		data: $('#FormTambahgolongan').serialize(),
		dataType:'json',
		success: function(json){
			if(json.status == 1){ 
				$('#ResponseInput').html(json.pesan);
				setTimeout(function(){ 
			   		$('#ResponseInput').html('');
			    }, 3000);
				$('#my-grid').DataTable().ajax.reload( null, false );

				$('#FormTambahgolongan').each(function(){
					this.reset();
				});
			}
			else {
				$('#ResponseInput').html(json.pesan);
			}
		}
	});
}

$(document).ready(function(){
	var Tombol = "<button type='button' class='btn btn-primary' id='SimpanTambahgolongan'>Simpan Data</button>";
	Tombol += "<button type='button' class='btn btn-default' data-dismiss='modal'>Tutup</button>";
	$('#ModalFooter').html(Tombol);

	$("#FormTambahgolongan").find('input[type=text],textarea,select').filter(':visible:first').focus();

	$('#SimpanTambahgolongan').click(function(e){
		e.preventDefault();
		Tambahgolongan();
	});

	$('#FormTambahgolongan').submit(function(e){
		e.preventDefault();
		Tambahgolongan();
	});
});
</script>