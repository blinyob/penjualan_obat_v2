<?php echo form_open('obat/edit-golongan/'.$golongan->id_merk_obat, array('id' => 'FormEditgolongan')); ?>
<div class='form-group'>
	<?php
	echo form_input(array(
		'name' => 'golongan', 
		'class' => 'form-control',
		'value' => $golongan->merk
	));
	?>
</div>
<?php echo form_close(); ?>

<div id='ResponseInput'></div>

<script>
function Editgolongan()
{
	$.ajax({
		url: $('#FormEditgolongan').attr('action'),
		type: "POST",
		cache: false,
		data: $('#FormEditgolongan').serialize(),
		dataType:'json',
		success: function(json){
			if(json.status == 1){ 
				$('#ResponseInput').html(json.pesan);
				setTimeout(function(){ 
			   		$('#ResponseInput').html('');
			    }, 3000);
				$('#my-grid').DataTable().ajax.reload( null, false );
			}
			else {
				$('#ResponseInput').html(json.pesan);
			}
		}
	});
}

$(document).ready(function(){
	var Tombol = "<button type='button' class='btn btn-primary' id='SimpanEditgolongan'>Update Data</button>";
	Tombol += "<button type='button' class='btn btn-default' data-dismiss='modal'>Tutup</button>";
	$('#ModalFooter').html(Tombol);

	$("#FormEditgolongan").find('input[type=text],textarea,select').filter(':visible:first').focus();

	$('#SimpanEditgolongan').click(function(e){
		e.preventDefault();
		Editgolongan();
	});

	$('#FormEditgolongan').submit(function(e){
		e.preventDefault();
		Editgolongan();
	});
});
</script>