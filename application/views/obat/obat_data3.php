<?php $this->load->view('template/head'); ?>
<?php $this->load->view('template/topbar'); ?>
<?php $this->load->view('template/sidebar'); ?>

<?php
$level = $this->session->userdata('ap_level');
?>

<section class="content">
    <!-- Info boxes -->

    
    <div class="panel panel-default">
		<div class="panel-body">
        <div class="info-box">
			<h5><i class='fa fa-cube fa-fw'></i> obat <i class='fa fa-angle-right fa-fw'></i> Semua obat</h5>
			<hr />
			<?php echo form_open('obat/tambah', array('id' => 'FormTambahobat')); ?>
			<div id='ResponseInput'></div>

<div class="col-md-6" id="TabelTambahObat">
			<div class='form-group'>
	<label>Nama Obat</label>
	<input type='text' name='nama' class='form-control'>
</div>
<div class='form-group'>
	<label>Kategori</label>
	<select name='id_kategori_obat' class='form-control'>
	<option value=""></option>
	<?php
	foreach($kategori->result() as $k)
	{
		echo "<option value='".$a->id_kategori_obat."'>".$k->kategori."</option>";
	}
	?>
	</select>
</div>
<div class='form-group'>
	<label>Batch</label>
	<select name='id_golongan_obat' class='form-control'>
	<option value=""></option>
	<?php
	foreach($golongan->result() as $g)
	{
		echo "<option value='".$g->id_golongan_obat."'>".$g->golongan."</option>";
	}
	?>
	</select>
</div>
<div class='form-group'>
	<label>Satuan</label>
	<select name='id_satuan_obat' class='form-control'>
	<option value=""></option>
	<?php
	foreach($satuan->result() as $s)
	{
		echo "<option value='".$s->id_satuan_obat."'>".$s->satuan."</option>";
	}
	?>
	</select>
</div>


<div class='form-group'>
	<label>Stok</label>
	<input type='text' name='stok' class='form-control'>
</div>
</div>
<div class="col-md-6">
<div class='form-group'>
	<label>Harga</label>
	<input type='text' name='harga' class='form-control'>
</div>

<div class='form-group'>
	<label>Tanggal Pembuatan</label>
	<input type='text' name='tgl_pembuatan' class='form-control tgl_pembuatan'>
</div>
<div class='form-group'>
	<label>Tanggal Kadaluarsa</label>
	<input type='text' name='tgl_kad' class='form-control tgl_kad'>
</div>
<div class='form-group'>
	<label>Keterangan</label>
	<textarea name='keterangan' class='form-control'></textarea>
</div>
</div>
<?php echo form_close(); ?>
&nbsp&nbsp&nbsp&nbsp<button type='button' class='btn btn-primary' id='SimpanTambahobat'>Simpan Data</button>
<hr />
			<div class='table-responsive'>

				    <table id="table-data example1" class="table table-bordered table-striped">
				     <thead>
						<tr>
							<th>Kode</th>
							<th>Nama obat</th>
							<th>Kategori</th>
							<th>Batch</th>
							<th>Satuan</th>
							<th>Stok</th>
							<th>Harga</th>
							<th>Tanggal Pembuatan</th>
							<th>Tanggal Kadaluarsa</th>
							
							<th>Keterangan</th>
							<?php if($level == 'admin' OR $level == 'inventory') { ?>
							<th class='no-sort'>Hapus</th>
							<?php } ?>
						</tr>
				     </thead>
					<tbody id="table-body">
				     	<?php /* $i=1;
				     	 foreach ($obat as $dt_obat) { ?>
				     	 	<tr>
				     	 	  <td><?php echo $i++;?></td>
					          <td class="table_data" data-row_id="<?php echo $dt_obat['kode_obat'] ?>" data-column_name="kode_obat" contenteditable><?php echo $dt_obat['kode_obat']; ?></td>
					          <td class="table_data" data-row_id="<?php echo $dt_obat['kode_obat'] ?>" data-column_name="nama_obat" contenteditable><?php echo $dt_obat['nama_obat']; ?></td>
					          <td class="table_data" data-row_id="<?php echo $dt_obat['kode_obat'] ?>" data-column_name="kategori" contenteditable><?php echo $dt_obat['kategori']; ?></td>
					          <td class="table_data" data-row_id="<?php echo $dt_obat['kode_obat'] ?>" data-column_name="golongan" contenteditable><?php echo $dt_obat['golongan']; ?></td>
					          <td class="table_data" data-row_id="<?php echo $dt_obat['kode_obat'] ?>" data-column_name="satuan" contenteditable><?php echo $dt_obat['satuan']; ?></td>
					          <td class="table_data" data-row_id="<?php echo $dt_obat['kode_obat'] ?>" data-column_name="total_stock" contenteditable><?php echo $dt_obat['total_stok']; ?></td>
					          <td class="table_data" data-row_id="<?php echo $dt_obat['kode_obat'] ?>" data-column_name="harga" contenteditable><?php echo $dt_obat['harga']; ?></td>
					          <td class="table_data" data-row_id="<?php echo $dt_obat['kode_obat'] ?>" data-column_name="tgl_pembuatan" contenteditable><?php echo $dt_obat['tgl_pembuatan']; ?></td>
					          <td class="table_data" data-row_id="<?php echo $dt_obat['kode_obat'] ?>" data-column_name="total_stock" contenteditable><?php echo $dt_obat['tgl_kad']; ?></td>
					          <td class="table_data" data-row_id="<?php echo $dt_obat['kode_obat'] ?>" data-column_name="total_stock" contenteditable><?php echo $dt_obat['keterangan']; ?></td>
					          <td><button type="button" name="delete_btn" id="<?php echo $dt_obat['kode_obat']; ?>" class="btn btn-xs btn-danger btn_delete"><span class="glyphicon glyphicon-remove"></span></button></td>
					         </tr>
				     	<?php } */?>
				     </tbody>
				    </table>

			</div>
		</div>
	</div>
</div>
</section>
<script type="text/javascript">

  function load_data()
  {
    $.ajax({
      url:"<?php echo base_url(); ?>obat/load_obat",
      dataType:"JSON",
      success:function(data){
        var html = '';
        for(var count = 0; count < data.length; count++)
        {
          html += '<tr>';
		  html += '<td class="table_data" data-row_id="'+data[count].kode_obat+'" data-column_name="kode_obat" contenteditable>'+data[count].kode_obat+'</td>';
          html += '<td class="table_data" data-row_id="'+data[count].kode_obat+'" data-column_name="nama_obat" contenteditable>'+data[count].nama_obat+'</td>';
	   	  html += '<td class="table_data" data-row_id="'+data[count].kode_obat+'" data-column_name="kategori" contenteditable>'+data[count].kategori+'</td>';
		  html +=  '<td class="table_data" data-row_id="'+data[count].kode_obat+'" data-column_name="golongan" contenteditable>'+data[count].golongan+'</td>';
		  html +=  '<td class="table_data" data-row_id="'+data[count].kode_obat+'" data-column_name="satuan" contenteditable>'+data[count].satuan+'</td>';
		  html +=  '<td class="table_data" data-row_id="'+data[count].kode_obat+'" data-column_name="total_stock" contenteditable>'+data[count].total_stok+'</td>';
		  html +=  '<td class="table_data" data-row_id="'+data[count].kode_obat+'" data-column_name="harga" contenteditable>'+data[count].harga+'</td>';
		  html +=  '<td class="table_data" data-row_id="'+data[count].kode_obat+'" data-column_name="tgl_pembuatan" contenteditable>'+data[count].tgl_pembuatan+'</td>';
		  html +=  '<td class="table_data" data-row_id="'+data[count].kode_obat+'" data-column_name="total_stock" contenteditable>'+data[count].tgl_kad+'</td>';
		  html +=  '<td class="table_data" data-row_id="'+data[count].kode_obat+'" data-column_name="total_stock" contenteditable>'+data[count].keterangan+'</td>';
/*          html += '<td class="table_data" data-row_id="'+data[count].id+'" data-column_name="first_name" contenteditable>'+data[count].first_name+'</td>';
          html += '<td class="table_data" data-row_id="'+data[count].id+'" data-column_name="last_name" contenteditable>'+data[count].last_name+'</td>';
          html += '<td class="table_data" data-row_id="'+data[count].id+'" data-column_name="age" contenteditable>'+data[count].age+'</td>';
          html += '<td><button type="button" name="delete_btn" id="'+data[count].id+'" class="btn btn-xs btn-danger btn_delete"><span class="glyphicon glyphicon-remove"></span></button></td></tr>';
*/        html += '</tr>';  
		}
        $('tbody').html(html);
      }
    });
  }

  load_data();

  $(document).on('blur', '.table_data', function(){
    var id = $(this).data('row_id');
    var table_column = $(this).data('column_name');
    var value = $(this).text();
    console.log(value)
    $.ajax({
      url:"<?php echo site_url(); ?>/obat/edit_obat2",
      method:"POST",
      data:{id:id, table_column:table_column, value:value},
      success:function(data)
      {
        load_data();
      },
	error:function(jqXHR, textStatus, errorThrown) {
     //tampilkan kode error
     alert('Error : '+jqXHR.status);
     }      
    })
  });
</script>
<link rel="stylesheet" type="text/css" href="<?php echo config_item('plugin'); ?>datetimepicker/jquery.datetimepicker.css"/>
<script src="<?php echo config_item('plugin'); ?>datetimepicker/jquery.datetimepicker.js"></script>
    <!-- DATA TABES SCRIPT -->
    <script src="../../plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <!-- page script -->
    <script type="text/javascript">
      $(function () {
        $("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
      });
    </script>
<?php $this->load->view('template/js'); ?>
<?php $this->load->view('template/foot'); ?>
