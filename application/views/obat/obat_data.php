<?php $this->load->view('template/head'); ?>
<?php $this->load->view('template/topbar'); ?>
<?php $this->load->view('template/sidebar'); ?>

<?php
$level = $this->session->userdata('ap_level');
?>

<section class="content">
    <!-- Info boxes -->

    
    <div class="panel panel-default">
		<div class="panel-body">
        <div class="info-box">
			<h5><i class='fa fa-cube fa-fw'></i> obat <i class='fa fa-angle-right fa-fw'></i> Semua obat</h5>
			<hr />
			<?php echo form_open('obat/tambah', array('id' => 'FormTambahobat')); ?>
			<div id='ResponseInput'></div>

<div class="col-md-6" id="TabelTambahObat">
			<div class='form-group'>
	<label>Nama Obat</label>
	<input type='text' name='nama' class='form-control'>
</div>
<div class='form-group'>
	<label>Kategori</label>
	<select name='id_kategori_obat' class='form-control'>
	<option value=""></option>
	<?php
	foreach($kategori->result() as $k)
	{
		echo "<option value='".$a->id_kategori_obat."'>".$k->kategori."</option>";
	}
	?>
	</select>
</div>
<div class='form-group'>
	<label>Batch</label>
	<select name='id_golongan_obat' class='form-control'>
	<option value=""></option>
	<?php
	foreach($golongan->result() as $g)
	{
		echo "<option value='".$g->id_golongan_obat."'>".$g->golongan."</option>";
	}
	?>
	</select>
</div>
<div class='form-group'>
	<label>Satuan</label>
	<select name='id_satuan_obat' class='form-control'>
	<option value=""></option>
	<?php
	foreach($satuan->result() as $s)
	{
		echo "<option value='".$s->id_satuan_obat."'>".$s->satuan."</option>";
	}
	?>
	</select>
</div>


<div class='form-group'>
	<label>Stok</label>
	<input type='text' name='stok' class='form-control'>
</div>
</div>
<div class="col-md-6">
<div class='form-group'>
	<label>Harga</label>
	<input type='text' name='harga' class='form-control'>
</div>

<div class='form-group'>
	<label>Tanggal Pembuatan</label>
	<input type='text' name='tgl_pembuatan' class='form-control tgl_pembuatan'>
</div>
<div class='form-group'>
	<label>Tanggal Kadaluarsa</label>
	<input type='text' name='tgl_kad' class='form-control tgl_kad'>
</div>
<div class='form-group'>
	<label>Keterangan</label>
	<textarea name='keterangan' class='form-control'></textarea>
</div>
</div>
<?php echo form_close(); ?>
&nbsp&nbsp&nbsp&nbsp<button type='button' class='btn btn-primary' id='SimpanTambahobat'>Simpan Data</button>
<hr />
			<div class='table-responsive'>
				<link rel="stylesheet" href="<?php echo config_item('plugin'); ?>datatables/css/dataTables.bootstrap.css"/>
				<table id="my-grid" class="table table-striped table-bordered" width="100%">
					<thead>
						<tr>
							<th>#</th>
							<th>Kode</th>
							<th>Nama obat</th>
							<th>Kategori</th>
							<th>Batch</th>
							<th>Satuan</th>
							<th>Stok</th>
							<th>Harga</th>
							<th>Tanggal Pembuatan</th>
							<th>Tanggal Kadaluarsa</th>
							
							<th>Keterangan</th>
							<?php if($level == 'admin' OR $level == 'inventory') { ?>
							<th class='no-sort'>Hapus</th>
							<?php } ?>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
</section>

<link rel="stylesheet" type="text/css" href="<?php echo config_item('plugin'); ?>datetimepicker/jquery.datetimepicker.css"/>
<script src="<?php echo config_item('plugin'); ?>datetimepicker/jquery.datetimepicker.js"></script>
<script>
$('.tgl_pembuatan').datetimepicker({
	lang:'en',
	timepicker:false,
	format:'Y-m-d',
	closeOnDateSelect:true
});
$('.tgl_kad').datetimepicker({
	lang:'en',
	timepicker:false,
	format:'Y-m-d',
	closeOnDateSelect:true
});
$(document).ready(function(){

	$('#SimpanTambahobat').click(function(e){
		e.preventDefault();

		if($(this).hasClass('disabled'))
		{
			return false;
		}
		else
		{
			if($('#FormTambahobat').serialize() !== '')
			{
				$.ajax({
					url: $('#FormTambahobat').attr('action'),
					type: "POST",
					cache: false,
					data: $('#FormTambahobat').serialize(),
					dataType:'json',
					beforeSend:function(){
						$('#SimpanTambahobat').html("Menyimpan Data, harap tunggu ...");
					},
					success: function(json){
						if(json.status == 1){ 
							$('.modal-dialog').removeClass('modal-lg');
							$('.modal-dialog').addClass('modal-sm');
							$('#ModalHeader').html('Sukses !');
							$('#ModalContent').html(json.pesan);
							$('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal'>Ok</button>");
							$('#ModalGue').modal('show');
							$('#my-grid').DataTable().ajax.reload( null, false );
						}
						else {
							$('#ResponseInput').html(json.pesan);
						}

						$('#SimpanTambahobat').html('Simpan Data');
					}
				});
			}
			else
			{
				$('#ResponseInput').html('');
			}
		}
	});

	$("#FormTambahobat").find('input[type=text],textarea,select').filter(':visible:first').focus();
});

$(document).on('click', '#HapusBaris', function(e){
	e.preventDefault();
	$(this).parent().parent().remove();

	var Nomor = 1;
	$('#TabelTambahobat tbody tr').each(function(){
		$(this).find('td:nth-child(1)').html(Nomor);
		Nomor++;
	});

	$('#SimpanTambahobat').removeClass('disabled');
});

function check_int(evt) {
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	return ( charCode >= 48 && charCode <= 57 || charCode == 8 );
}
</script>
<script type="text/javascript" language="javascript" >
	$(document).ready(function() {
		var dataTable = $('#my-grid').DataTable( {
			"serverSide": true,
			"stateSave" : false,
			"bAutoWidth": true,
			"oLanguage": {
				"sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
				"sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
				"sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
				"sInfoFiltered": "(difilter dari _MAX_ total data)", 
				"sZeroRecords": "Pencarian tidak ditemukan", 
				"sEmptyTable": "Data kosong", 
				"sLoadingRecords": "Harap Tunggu...", 
				"oPaginate": {
					"sPrevious": "Prev",
					"sNext": "Next"
				}
			},
			"aaSorting": [[ 0, "desc" ]],
			"columnDefs": [ 
				{
					"targets": 'no-sort',
					"orderable": false,
				}
	        ],
			"sPaginationType": "simple_numbers", 
			"iDisplayLength": 10,
			"aLengthMenu": [[10, 20, 50, 100, 150], [10, 20, 50, 100, 150]],
			"ajax":{
				url :"<?php echo site_url('obat/obat-json'); ?>",
				type: "post",
				error: function(){ 
					$(".my-grid-error").html("");
					$("#my-grid").append('<tbody class="my-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
					$("#my-grid_processing").css("display","none");
				}
			}
		} );
	});
	
	$(document).on('click', '#Hapusobat', function(e){
		e.preventDefault();
		var Link = $(this).attr('href');

		$('.modal-dialog').removeClass('modal-lg');
		$('.modal-dialog').addClass('modal-sm');
		$('#ModalHeader').html('Konfirmasi');
		$('#ModalContent').html('Apakah anda yakin ingin menghapus <br /><b>'+$(this).parent().parent().find('td:nth-child(3)').html()+'</b> ?');
		$('#ModalFooter').html("<button type='button' class='btn btn-primary' id='YesDelete' data-url='"+Link+"'>Ya, saya yakin</button><button type='button' class='btn btn-default' data-dismiss='modal'>Batal</button>");
		$('#ModalGue').modal('show');
	});

	$(document).on('click', '#YesDelete', function(e){
		e.preventDefault();
		$('#ModalGue').modal('hide');

		$.ajax({
			url: $(this).data('url'),
			type: "POST",
			cache: false,
			dataType:'json',
			success: function(data){
				$('#Notifikasi').html(data.pesan);
				$("#Notifikasi").fadeIn('fast').show().delay(3000).fadeOut('fast');
				$('#my-grid').DataTable().ajax.reload( null, false );
			}
		});
	});

	$(document).on('click', '#Tambahobat, #Editobat', function(e){
		e.preventDefault();
		if($(this).attr('id') == 'Tambahobat')
		{
			$('.modal-dialog').removeClass('modal-sm');
			$('.modal-dialog').addClass('modal-lg');
			$('#ModalHeader').html('Tambah obat');
		}
		if($(this).attr('id') == 'Editobat')
		{
			$('.modal-dialog').removeClass('modal-sm');
			$('.modal-dialog').removeClass('modal-lg');
			$('#ModalHeader').html('Edit obat');
		}
		$('#ModalContent').load($(this).attr('href'));
		$('#ModalGue').modal('show');
	});

	$(document).on('keyup', '.kode_obat', function(){
		$(this).parent().find('span').html("");

		var Kode = $(this).val();
		var Indexnya = $(this).parent().parent().index();
		var Pass = 0;
		$('#TabelTambahobat tbody tr').each(function(){
			if(Indexnya !== $(this).index())
			{
				var KodeLoop = $(this).find('td:nth-child(2) input').val();
				if(KodeLoop !== '')
				{
					if(KodeLoop == Kode){
						Pass++;
					}
				}
			}
		});

		if(Pass > 0)
		{
			$(this).parent().find('span').html("<font color='red'>Kode sudah ada</font>");
			$('#SimpanTambahobat').addClass('disabled');
		}
		else
		{
			$(this).parent().find('span').html('');
			$('#SimpanTambahobat').removeClass('disabled');

			$.ajax({
				url: "<?php echo site_url('obat/ajax-cek-kode'); ?>",
				type: "POST",
				cache: false,
				data: "kodenya="+Kode,
				dataType:'json',
				success: function(json){
					if(json.status == 0){ 
						$('#TabelTambahobat tbody tr:eq('+Indexnya+') td:nth-child(2)').find('span').html(json.pesan);
						$('#SimpanTambahobat').addClass('disabled');
					}
					if(json.status == 1){ 
						$('#SimpanTambahobat').removeClass('disabled');
					}
				}
			});
		}
	});
</script>
<?php $this->load->view('template/js'); ?>
<?php $this->load->view('template/foot'); ?>
