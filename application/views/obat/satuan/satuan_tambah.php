<?php echo form_open('obat/tambah-satuan', array('id' => 'FormTambahsatuan')); ?>
<div class='form-group'>
	<input type='text' name='satuan' class='form-control'>
</div>
<?php echo form_close(); ?>

<div id='ResponseInput'></div>

<script>
function Tambahsatuan()
{
	$.ajax({
		url: $('#FormTambahsatuan').attr('action'),
		type: "POST",
		cache: false,
		data: $('#FormTambahsatuan').serialize(),
		dataType:'json',
		success: function(json){
			if(json.status == 1){ 
				$('#ResponseInput').html(json.pesan);
				setTimeout(function(){ 
			   		$('#ResponseInput').html('');
			    }, 3000);
				$('#my-grid').DataTable().ajax.reload( null, false );

				$('#FormTambahsatuan').each(function(){
					this.reset();
				});
			}
			else {
				$('#ResponseInput').html(json.pesan);
			}
		}
	});
}

$(document).ready(function(){
	var Tombol = "<button type='button' class='btn btn-primary' id='SimpanTambahsatuan'>Simpan Data</button>";
	Tombol += "<button type='button' class='btn btn-default' data-dismiss='modal'>Tutup</button>";
	$('#ModalFooter').html(Tombol);

	$("#FormTambahsatuan").find('input[type=text],textarea,select').filter(':visible:first').focus();

	$('#SimpanTambahsatuan').click(function(e){
		e.preventDefault();
		Tambahsatuan();
	});

	$('#FormTambahsatuan').submit(function(e){
		e.preventDefault();
		Tambahsatuan();
	});
});
</script>