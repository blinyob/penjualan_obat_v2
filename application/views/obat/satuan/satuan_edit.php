<?php echo form_open('obat/edit-satuan/'.$satuan->id_satuan_obat, array('id' => 'FormEditsatuan')); ?>
<div class='form-group'>
	<?php
	echo form_input(array(
		'name' => 'satuan', 
		'class' => 'form-control',
		'value' => $satuan->satuan
	));
	?>
</div>
<?php echo form_close(); ?>

<div id='ResponseInput'></div>

<script>
function Editsatuan()
{
	$.ajax({
		url: $('#FormEditsatuan').attr('action'),
		type: "POST",
		cache: false,
		data: $('#FormEditsatuan').serialize(),
		dataType:'json',
		success: function(json){
			if(json.status == 1){ 
				$('#ResponseInput').html(json.pesan);
				setTimeout(function(){ 
			   		$('#ResponseInput').html('');
			    }, 3000);
				$('#my-grid').DataTable().ajax.reload( null, false );
			}
			else {
				$('#ResponseInput').html(json.pesan);
			}
		}
	});
}

$(document).ready(function(){
	var Tombol = "<button type='button' class='btn btn-primary' id='SimpanEditsatuan'>Update Data</button>";
	Tombol += "<button type='button' class='btn btn-default' data-dismiss='modal'>Tutup</button>";
	$('#ModalFooter').html(Tombol);

	$("#FormEditsatuan").find('input[type=text],textarea,select').filter(':visible:first').focus();

	$('#SimpanEditsatuan').click(function(e){
		e.preventDefault();
		Editsatuan();
	});

	$('#FormEditsatuan').submit(function(e){
		e.preventDefault();
		Editsatuan();
	});
});
</script>