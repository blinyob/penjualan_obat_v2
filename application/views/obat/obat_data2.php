<?php $this->load->view('template/head'); ?>
<?php $this->load->view('template/topbar'); ?>
<?php $this->load->view('template/sidebar'); ?>

<?php
$level = $this->session->userdata('ap_level');
?>

<section class="content">
    <!-- Info boxes -->

    
    <div class="panel panel-default">
		<div class="panel-body">
        <div class="info-box">
			<h5><i class='fa fa-cube fa-fw'></i> obat <i class='fa fa-angle-right fa-fw'></i> Semua obat</h5>
			<hr />
			<?php echo form_open('obat/tambah', array('id' => 'FormTambahobat')); ?>
			<div id='ResponseInput'></div>

<div class="col-md-6" id="TabelTambahObat">
			<div class='form-group'>
	<label>Nama Obat</label>
	<input type='text' name='nama' class='form-control'>
</div>
<div class='form-group'>
	<label>Kategori</label>
	<select name='id_kategori_obat' class='form-control'>
	<option value=""></option>
	<?php
	foreach($kategori->result() as $k)
	{
		echo "<option value='".$a->id_kategori_obat."'>".$k->kategori."</option>";
	}
	?>
	</select>
</div>
<div class='form-group'>
	<label>Batch</label>
	<select name='id_golongan_obat' class='form-control'>
	<option value=""></option>
	<?php
	foreach($golongan->result() as $g)
	{
		echo "<option value='".$g->id_golongan_obat."'>".$g->golongan."</option>";
	}
	?>
	</select>
</div>
<div class='form-group'>
	<label>Satuan</label>
	<select name='id_satuan_obat' class='form-control'>
	<option value=""></option>
	<?php
	foreach($satuan->result() as $s)
	{
		echo "<option value='".$s->id_satuan_obat."'>".$s->satuan."</option>";
	}
	?>
	</select>
</div>


<div class='form-group'>
	<label>Stok</label>
	<input type='text' name='stok' class='form-control'>
</div>
</div>
<div class="col-md-6">
<div class='form-group'>
	<label>Harga</label>
	<input type='text' name='harga' class='form-control'>
</div>

<div class='form-group'>
	<label>Tanggal Pembuatan</label>
	<input type='text' name='tgl_pembuatan' class='form-control tgl_pembuatan'>
</div>
<div class='form-group'>
	<label>Tanggal Kadaluarsa</label>
	<input type='text' name='tgl_kad' class='form-control tgl_kad'>
</div>
<div class='form-group'>
	<label>Keterangan</label>
	<textarea name='keterangan' class='form-control'></textarea>
</div>
</div>
<?php echo form_close(); ?>
&nbsp&nbsp&nbsp&nbsp<button type='button' class='btn btn-primary' id='SimpanTambahobat'>Simpan Data</button>
<hr />
		<span id='Notifikasi' style='display: none;'></span>
			<div class='table-responsive'>
				    <table id="example1" class="table table-bordered table-striped">
				     <thead>
						<tr>
							<th>#</th>
							<th>Kode</th>
							<th>Nama obat</th>
							<th>Kategori</th>
							<th>Batch</th>
							<th>Satuan</th>
							<th>Stok</th>
							<th>Harga</th>
							<th>Tanggal Pembuatan</th>
							<th>Tanggal Kadaluarsa</th>
							
							<th>Keterangan</th>
							<?php if($level == 'admin' OR $level == 'inventory') { ?>
							<th class='no-sort'>Hapus</th>
							<?php } ?>
						</tr>
				     </thead>
					<tbody>
				     	<?php $i=1;
				     	 foreach ($obat as $dt_obat) { ?>
				     	 	<tr>
				     	 	  <td><?php echo $i++;?></td>
					          <td><span class="span-kode_obat caption" data-id="<?php echo $dt_obat['kode_obat'];?>" ><?php echo $dt_obat['kode_obat'];?></span> <input type='text' class='field-kode_obat editor' value="<?php echo $dt_obat['kode_obat'];?>" data-id="<?php echo $dt_obat['kode_obat'];?>" /></td>
					          <td><span class="span-nama_obat caption" data-id="<?php echo $dt_obat['kode_obat'];?>" ><?php echo $dt_obat['nama_obat'];?></span> <input type='text' class='field-nama_obat editor' value="<?php echo $dt_obat['nama_obat'];?>" data-id="<?php echo $dt_obat['kode_obat'];?>" /></td>
					          <td><?php echo $dt_obat['kategori'];?></td>
					          <td><?php echo $dt_obat['golongan'];?></td>
					          <td><?php echo $dt_obat['satuan'];?></td>
					          <td><span class="span-total_stok caption" data-id="<?php echo $dt_obat['kode_obat'];?>" ><?php echo $dt_obat['total_stok'];?></span> <input type='text' class='field-stok editor' value="<?php echo $dt_obat['total_stok'];?>" data-id="<?php echo $dt_obat['kode_obat'];?>" /></td>
					          <td><span class="span-harga caption" data-id="<?php echo $dt_obat['kode_obat'];?>" ><?php echo $dt_obat['harga'];?></span> <input type='text' class='field-harga editor' value="<?php echo $dt_obat['harga'];?>" data-id="<?php echo $dt_obat['kode_obat'];?>" /></td>
					          <td><span class="span-tgl_pembuatan caption" data-id="<?php echo $dt_obat['kode_obat'];?>" ><?php echo $dt_obat['tgl_pembuatan'];?></span> <input type='text' class='field-tgl_pembuatan editor' value="<?php echo $dt_obat['tgl_pembuatan'];?>" data-id="<?php echo $dt_obat['kode_obat'];?>" /></td>
					          <td><span class="span-tgl_kad caption" data-id="<?php echo $dt_obat['kode_obat'];?>" ><?php echo $dt_obat['tgl_kad'];?></span> <input type='text' class='field-tgl_kad editor' value="<?php echo $dt_obat['tgl_kad'];?>" data-id="<?php echo $dt_obat['kode_obat'];?>" /></td>
					          <td><span class="span-keterangan caption" data-id="<?php echo $dt_obat['kode_obat'];?>" ><?php echo $dt_obat['keterangan'];?></span> <input type='text' class='field-keterangan editor' value="<?php echo $dt_obat['keterangan'];?>" data-id="<?php echo $dt_obat['kode_obat'];?>" /></td>

					          <td><button type="button" name="delete_btn" id="<?php echo $dt_obat['kode_obat']; ?>" class="btn btn-xs btn-danger btn_delete"><span class="glyphicon glyphicon-remove"></span></button></td>
					         </tr>
				     	<?php }?>
				     </tbody>
				    </table>
			</div>
		</div>
	</div>
</div>
</section>
<script type="text/javascript">
    $(function () {
		$("#example1").dataTable();
        $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
    });
</script>
<script type="text/javascript">

$(function(){

$.ajaxSetup({
	type:"post",
	cache:false,
	dataType: "json"
})


$(document).on("click","td",function(){
$(this).find("span[class~='caption']").hide();
$(this).find("input[class~='editor']").fadeIn().focus();
});


$(document).on("keydown",".editor",function(e){
if(e.keyCode==13){
var target=$(e.target);
var value=target.val();
var id=target.attr("data-id");
var data={id:id,value:value};
if(target.is(".field-kode_obat")){
data.modul="kode_obat";
}else if(target.is(".field-nama_obat")){
data.modul="nama_obat";
}else if(target.is(".field-kategori")){
data.modul="kategori";
}else if(target.is(".field-golongan")){
data.modul="golongan";
}else if(target.is(".field-satuan")){
data.modul="satuan";
}else if(target.is(".field-stok")){
data.modul="stok";
}else if(target.is(".field-harga")){
data.modul="harga";
}else if(target.is(".field-tgl_pembuatan")){
data.modul="tgl_pembuatan";
}else if(target.is(".field-tgl_kad")){
data.modul="tgl_kad";
}else if(target.is(".field-keterangan")){
data.modul="keterangan";
}
$.ajax({
	data:data,
	url :"<?php echo site_url(); ?>/obat/edit_obat/",
	success: function(data){
	 $('#Notifikasi').html("<font color='green'><i class='fa fa-check'></i> Data berhasil diubah !</font>");
	 $("#Notifikasi").fadeIn('fast').show().delay(3000).fadeOut('fast');
	 target.hide();
	 target.siblings("span[class~='caption']").html(value).fadeIn();
	},
	error:function(jqXHR, textStatus, errorThrown) {
     //tampilkan kode error
     alert('Error : '+jqXHR.status);
     }


})

}

});


$(document).on("click",".hapus-member",function(){
	var id=$(this).attr("data-id");
	swal({
		title:"Hapus Member",
		text:"Yakin akan menghapus member ini?",
		type: "warning",
		showCancelButton: true,
		confirmButtonText: "Hapus",
		closeOnConfirm: true,
	},
		function(){
		 $.ajax({
			url:"<?php echo base_url('index.php/crud/delete'); ?>",
			data:{id:id},
			success: function(){
				$("tr[data-id='"+id+"']").fadeOut("fast",function(){
					$(this).remove();
				});
			}
		 });
	});
});

});

</script>
<?php $this->load->view('template/js'); ?>
<?php $this->load->view('template/foot'); ?>
