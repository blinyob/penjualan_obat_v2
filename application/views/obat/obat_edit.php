<?php echo form_open('obat/edit/'.$obat->kode_obat, array('id' => 'FormEditobat')); ?>
<div class="form-horizontal">
	<div class="form-group">
		<label class="col-sm-3 control-label">Nama obat</label>
		<div class="col-sm-8">
			<?php 
			echo form_input(array(
				'name' => 'nama_obat',
				'class' => 'form-control',
				'value' => $obat->nama_obat
			));
			?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Kategori</label>
		<div class="col-sm-8">
			<select name='id_kategori_obat' class='form-control'>
				<option value=''></option>
				<?php
				foreach($kategori->result() as $k)
				{
					$selected = '';
					if($obat->id_kategori_obat == $k->id_kategori_obat){
						$selected = 'selected';
					}
					
					echo "<option value='".$k->id_kategori_obat."' ".$selected.">".$k->kategori."</option>";
				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Golongan</label>
		<div class="col-sm-8">
			<select name='id_golongan_obat' class='form-control'>
				<option value=''></option>
				<?php
				foreach($golongan->result() as $g)
				{
					$selected = '';
					if($obat->id_golongan_obat == $g->id_golongan_obat){
						$selected = 'selected';
					}
					
					echo "<option value='".$g->id_golongan_obat."' ".$selected.">".$g->golongan."</option>";
				}
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Satuan</label>
		<div class="col-sm-8">
			<select name='id_satuan_obat' class='form-control'>
				<option value=''></option>
				<?php
				foreach($satuan->result() as $s)
				{
					$selected = '';
					if($obat->id_satuan_obat == $s->id_satuan_obat){
						$selected = 'selected';
					}
					
					echo "<option value='".$s->id_satuan_obat."' ".$selected.">".$s->satuan."</option>";
				}
				?>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label">Stok</label>
		<div class="col-sm-8">
			<?php 
			echo form_input(array(
				'name' => 'total_stok',
				'class' => 'form-control',
				'value' => $obat->total_stok
			));
			?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Harga</label>
		<div class="col-sm-8">
			<?php 
			echo form_input(array(
				'name' => 'harga',
				'class' => 'form-control',
				'value' => $obat->harga
			));
			?>
		</div>
	</div>
	<div class="form-horizontal">
	<div class="form-group">
		<label class="col-sm-3 control-label">Tanggal Pembuatan</label>
		<div class="col-sm-8">
			<?php 
			echo form_input(array(
				'name' => 'nama_obat',
				'class' => 'form-control',
				'value' => $obat->tgl_pembuatan
			));
			?>
		</div>
	</div>
	<div class="form-horizontal">
	<div class="form-group">
		<label class="col-sm-3 control-label">Tanggal Kadaluarsa</label>
		<div class="col-sm-8">
			<?php 
			echo form_input(array(
				'name' => 'nama_obat',
				'class' => 'form-control',
				'value' => $obat->tgl_kad
			));
			?>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Keterangan</label>
		<div class="col-sm-8">
			<textarea name='keterangan' class='form-control' rows='3' style='resize:vertical;'><?php echo $obat->keterangan; ?></textarea>
		</div>
	</div>
</div>
<?php echo form_close(); ?>

<div id='ResponseInput'></div>

<script>
$(document).ready(function(){
	var Tombol = "<button type='button' class='btn btn-primary' id='SimpanEditobat'>Update Data</button>";
	Tombol += "<button type='button' class='btn btn-default' data-dismiss='modal'>Tutup</button>";
	$('#ModalFooter').html(Tombol);

	$('#SimpanEditobat').click(function(){
		$.ajax({
			url: $('#FormEditobat').attr('action'),
			type: "POST",
			cache: false,
			data: $('#FormEditobat').serialize(),
			dataType:'json',
			success: function(json){
				if(json.status == 1){ 
					$('#ResponseInput').html(json.pesan);
					setTimeout(function(){ 
				   		$('#ResponseInput').html('');
				    }, 3000);
					$('#my-grid').DataTable().ajax.reload( null, false );
				}
				else {
					$('#ResponseInput').html(json.pesan);
				}
			}
		});
	});
});
</script>