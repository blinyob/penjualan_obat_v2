<?php echo form_open('distributor/distributor-edit/'.$distributor->id_distributor, array('id' => 'FormEditdistributor')); ?>

<div class='form-group'>
	<label>Nama</label>
	<?php
	echo form_input(array(
		'name' => 'nama', 
		'class' => 'form-control',
		'value' => $distributor->nama
	));
	?>
</div>
<div class='form-group'>
	<label>Alamat</label>
	<?php
	echo form_textarea(array(
		'name' => 'alamat', 
		'class' => 'form-control',
		'value' => $distributor->alamat,
		'style' => "resize:vertical",
		'rows' => 3
	));
	?>
</div>
<div class='form-group'>
	<label>Nomor Telepon / Handphone</label>
	<?php
	echo form_input(array(
		'name' => 'telepon', 
		'class' => 'form-control',
		'value' => $distributor->telp
	));
	?>
</div>
<div class='form-group'>
	<label>Info Tambahan Lainnya</label>
	<?php
	echo form_textarea(array(
		'name' => 'info', 
		'class' => 'form-control',
		'value' => $distributor->info_tambahan,
		'style' => "resize:vertical",
		'rows' => 3
	));
	?>
</div>

<?php echo form_close(); ?>

<div id='ResponseInput'></div>

<script>
function Editdistributor()
{
	$.ajax({
		url: $('#FormEditdistributor').attr('action'),
		type: "POST",
		cache: false,
		data: $('#FormEditdistributor').serialize(),
		dataType:'json',
		success: function(json){
			if(json.status == 1){ 
				$('#ResponseInput').html(json.pesan);
				setTimeout(function(){ 
			   		$('#ResponseInput').html('');
			    }, 3000);
				$('#my-grid').DataTable().ajax.reload( null, false );
			}
			else {
				$('#ResponseInput').html(json.pesan);
			}
		}
	});
}

$(document).ready(function(){
	var Tombol = "<button type='button' class='btn btn-primary' id='SimpanEditdistributor'>Update Data</button>";
	Tombol += "<button type='button' class='btn btn-default' data-dismiss='modal'>Tutup</button>";
	$('#ModalFooter').html(Tombol);

	$("#FormEditdistributor").find('input[type=text],textarea,select').filter(':visible:first').focus();

	$('#SimpanEditdistributor').click(function(e){
		e.preventDefault();
		Editdistributor();
	});

	$('#FormEditdistributor').submit(function(e){
		e.preventDefault();
		Editdistributor();
	});
});
</script>