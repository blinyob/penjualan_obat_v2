<?php echo form_open('distributor/tambah-distributor', array('id' => 'FormTambahdistributor')); ?>
<div class='form-group'>
	<label>Nama</label>
	<input type='text' name='nama' class='form-control'>
</div>
<div class='form-group'>
	<label>Alamat</label>
	<textarea name='alamat' class='form-control' style='resize:vertical;'></textarea>
</div>
<div class='form-group'>
	<label>Nomor Telepon / Handphone</label>
	<input type='text' name='telepon' class='form-control'>
</div>
<div class='form-group'>
	<label>Info Tambahan Lainnya</label>
	<textarea name='info' class='form-control' style='resize:vertical;'></textarea>
</div>
<?php echo form_close(); ?>

<div id='ResponseInput'></div>

<script>
function Tambahdistributor()
{
	$.ajax({
		url: $('#FormTambahdistributor').attr('action'),
		type: "POST",
		cache: false,
		data: $('#FormTambahdistributor').serialize(),
		dataType:'json',
		success: function(json){
			if(json.status == 1)
			{ 
				$('#FormTambahdistributor').each(function(){
					this.reset();
				});

				if(document.getElementById('distributorArea') != null)
				{
					$('#ResponseInput').html('');

					$('.modal-dialog').removeClass('modal-lg');
					$('.modal-dialog').addClass('modal-sm');
					$('#ModalHeader').html('Berhasil');
					$('#ModalContent').html(json.pesan);
					$('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Okay</button>");
					$('#ModalGue').modal('show');

					$('#id_distributor').append("<option value='"+json.id_distributor+"' selected>"+json.nama+"</option>");
					$('#telp_distributor').html(json.telepon);
					$('#alamat_distributor').html(json.alamat);
					$('#info_tambahan_distributor').html(json.info);
				}
				else
				{
					$('#ResponseInput').html(json.pesan);
					setTimeout(function(){ 
				   		$('#ResponseInput').html('');
				    }, 3000);
					$('#my-grid').DataTable().ajax.reload( null, false );
				}
			}
			else 
			{
				$('#ResponseInput').html(json.pesan);
			}
		}
	});
}

$(document).ready(function(){
	var Tombol = "<button type='button' class='btn btn-primary' id='SimpanTambahdistributor'>Simpan Data</button>";
	Tombol += "<button type='button' class='btn btn-default' data-dismiss='modal'>Tutup</button>";
	$('#ModalFooter').html(Tombol);

	$("#FormTambahdistributor").find('input[type=text],textarea,select').filter(':visible:first').focus();

	$('#SimpanTambahdistributor').click(function(e){
		e.preventDefault();
		Tambahdistributor();
	});

	$('#FormTambahdistributor').submit(function(e){
		e.preventDefault();
		Tambahdistributor();
	});
});
</script>