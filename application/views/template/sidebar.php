<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/dist/img/user2-160x160.jpg') ?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                    <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
            <a href="<?php echo site_url('dashboard') ?>">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
                </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-shopping-cart"></i> <span>Transaksi</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('penjualan') ?>"><i class="fa fa-circle-o"></i>Penjualan</a></li>
                    <li><a href="<?php echo site_url('pembelian') ?>"><i class="fa fa-circle-o"></i>Pembelian</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i> <span>History Transaksi</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                <li><a href="<?php echo site_url('penjualan/history') ?>"><i class="fa fa-circle-o"></i>History Penjualan</a></li>
                    <li><a href="<?php echo site_url('pembelian/history') ?>"><i class="fa fa-circle-o"></i>History Pembelian</a></li>
                </ul>
            </li>
            <li class="treeview">
            <a href="#">
                    <i class="fa fa-cube"></i> <span>Data Obat</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                <li><a href="<?php echo site_url('obat') ?>"><i class="fa fa-circle-o"></i>List Obat</a></li>
                    <li><a href="<?php echo site_url('obat/list-kategori') ?>"><i class="fa fa-circle-o"></i>Kategori</a></li>
                    <li><a href="<?php echo site_url('obat/list-golongan') ?>"><i class="fa fa-circle-o"></i>Golongan</a></li>
                    <li><a href="<?php echo site_url('obat/list-satuan') ?>"><i class="fa fa-circle-o"></i>Satuan</a></li>
                </ul>
          </li>
          <li class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i> <span>Laporan</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('laporan') ?>"><i class="fa fa-circle-o"></i>Laporan Penjualan</a></li>
                    <li><a href="<?php echo site_url('laporan/pembelian') ?>"><i class="fa fa-circle-o"></i>Laporan Pembelian</a></li>
                </ul>
            </li>

          <li>
            <a href="<?php echo site_url('pasien') ?>">
                    <i class="fa fa-user"></i> <span>Data Pasien</span>
                    
                </a>
                </li>
                <li>
            <a href="<?php echo site_url('distributor') ?>">
                    <i class="fa fa-user"></i> <span>Data Distributor</span>
                    
                </a>
                </li>
    
                <li>
            <a href="<?php echo site_url('user') ?>">
                    <i class="fa fa-user"></i> <span>List User</span>
                </a>
                </li>
         
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">