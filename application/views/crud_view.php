<?php $this->load->view('template/head'); ?>
<?php $this->load->view('template/topbar'); ?>
<?php $this->load->view('template/sidebar'); ?>


	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/sweetalert/sweetalert.css'); ?>">

<script type="text/javascript" src="<?php echo base_url('assets/sweetalert/sweetalert.min.js'); ?>"></script>


<style type="text/css">

td {
	cursor: pointer;
}

.editor{
	display: none;
}

</style>


<div class="container">

<div class="row">
<div class="col-md-12">

<h2>Latihan live CRUD</h2>


<br>
<br>
<br>
<table id="table-data" class="table table-striped">

<thead>
<tr>
<th>Kode</th>
<th>Nama</th>
<th>Stok</th>
<th>Harga</th>
<th>Kategori</th>
<th>Golongan</th>
<th>Satuan</th>
<th>Tanggal Buat</th>
<th>Tanggal Kadaluarsa</th>

<th>Hapus</th>
</tr>
</thead>

<tbody>
<?php 

foreach ($people as $member) {
	echo "<tr data-kode_barang='$member[kode_barang]'>
			<td><span class='span-kode_barang caption' data-kode_barang='$member[kode_barang]'>$member[kode_barang]</span> <input type='text' class='field-kode_barang form-control editor' value='$member[kode_barang]' data-kode_barang='$member[kode_barang]' /></td>
			<td><span class='span-nama_barang caption' data-kode_barang='$member[kode_barang]'>$member[nama_barang]</span> <input type='text' class='field-nama_barang form-control editor' value='$member[nama_barang]' data-kode_barang='$member[kode_barang]' /></td>
			<td><span class='span-total_stok caption' data-kode_barang='$member[kode_barang]'>$member[total_stok]</span> <input type='text' class='field-total_stok form-control editor' value='$member[total_stok]' data-kode_barang='$member[kode_barang]' /></td>
			<td><span class='span-harga caption' data-kode_barang='$member[kode_barang]'>$member[harga]</span> <input type='text' class='field-harga form-control editor' value='$member[harga]' data-kode_barang='$member[kode_barang]' /></td>
			<td><span class='span-id_kategori_obat caption' data-kode_barang='$member[kode_barang]'>$member[id_kategori_obat]</span> <input type='text' class='field-id_kategori_obat form-control editor' value='$member[id_kategori_obat]' data-kode_barang='$member[kode_barang]' /></td>
			<td><span class='span-id_golongan_obat caption' data-kode_barang='$member[kode_barang]'>$member[id_golongan_obat]</span> <input type='text' class='field-id_golongan_obat form-control editor' value='$member[id_golongan_obat]' data-kode_barang='$member[kode_barang]' /></td>
			<td><span class='span-id_satuan_obat' data-kode_barang='$member[kode_barang]'>$member[id_satuan_obat]</span> <input type='text' class='field-id_satuan_obat form-control editor' value='$member[id_satuan_obat]' data-kode_barang='$member[kode_barang]' /></td>
			<td><span class='span-tgl_pembuatan' data-kode_barang='$member[kode_barang]'>$member[tgl_pembuatan]</span> <input type='text' class='field-tgl_pembuatan form-control editor' value='$member[tgl_pembuatan]' data-kode_barang='$member[kode_barang]' /></td>
			<td><span class='span-tgl_kad' data-kode_barang='$member[kode_barang]'>$member[tgl_kad]</span> <input type='text' class='field-tgl_kad form-control editor' value='$member[tgl_kad]' data-kode_barang='$member[kode_barang]' /></td>
		
			<td><button class='btn btn-xs btn-danger hapus-member' data-kode_barang='$member[kode_barang]'><i class='glyphicon glyphicon-remove'></i> Hapus</button></td>
		
			</tr>";
}


 ?>
</tbody>

</table>

</div>
</div>

</div>






<script type="text/javascript">

$(function(){

$.ajaxSetup({
	type:"post",
	cache:false,
	dataType: "json"
})


$(document).on("click","td",function(){
$(this).find("span[class~='caption']").hide();
$(this).find("input[class~='editor']").fadeIn().focus();
});


$(document).on("keydown",".editor",function(e){
if(e.keyCode==13){
var target=$(e.target);
var value=target.val();
var kode_barang=target.attr("data-kode_barang");
var data={kode_barang:kode_barang,value:value};
if(target.is(".field-nama_barang")){
data.modul="nama_barang";
}else if(target.is(".field-total_stok")){
data.modul="total_stok";
}else if(target.is(".field-harga")){
data.modul="harga";
}else if(target.is(".field-id_kategori_obat")){
data.modul="id_kategpori_obat";
}else if(target.is(".field-id_golongan_obat")){
data.modul="id_golongan_obat";
}else if(target.is(".field-id_satuan_obat")){
data.modul="id_satuan_obat";
}else if(target.is(".field-tgl_pembuatan")){
data.modul="tgl_pembuatan";
}else if(target.is(".field-tgl_kad")){
data.modul="tgl_kad";
}

$.ajax({
	data:data,
	url:"<?php echo base_url('index.php/crud/update'); ?>",
	success: function(a){
	 target.hide();
	 target.siblings("span[class~='caption']").html(value).fadeIn();
	}

})

}

});


$(document).on("click",".hapus-member",function(){
	var kode_barang=$(this).attr("data-kode_barang");
	swal({
		title:"Hapus Member",
		text:"Yakin akan menghapus member ini?",
		type: "warning",
		showCancelButton: true,
		confirmButtonText: "Hapus",
		closeOnConfirm: true,
	},
		function(){
		 $.ajax({
			url:"<?php echo base_url('index.php/crud/delete'); ?>",
			data:{kode_barang:kode_barang},
			success: function(){
				$("tr[data-kode_barang='"+kode_barang+"']").fadeOut("fast",function(){
					$(this).remove();
				});
			}
		 });
	});
});
});
$(document).ready(function() {
		var table= $('#table-data').DataTable();
		});
</script>

<?php $this->load->view('template/js'); ?>
<?php $this->load->view('template/foot'); ?>
