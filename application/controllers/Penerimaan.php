
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ------------------------------------------------------------------------
 * CLASS NAME : obat
 * ------------------------------------------------------------------------
 *
 * @author     Muhammad Akbar <muslim.politekniktelkom@gmail.com>
 * @copyright  2016
 * @license    http://aplikasiphp.net
 *
 */

class Penerimaan extends MY_Controller 
{

	public function index()
	{
		$this->tambah();
	}
	public function transaksi()
	{
		$level = $this->session->userdata('ap_level');
		if($level == 'admin' OR $level == 'kasir')
		{
			if($_POST)
			{
				if( ! empty($_POST['kode_obat']))
				{
					$total = 0;
					foreach($_POST['kode_obat'] as $k)
					{
						if( ! empty($k)){ $total++; }
					}

					if($total > 0)
					{
						$this->load->library('form_validation');
						$this->form_validation->set_rules('nomor_nota','Nomor Nota','trim|required|max_length[40]|alpha_numeric|callback_cek_nota[nomor_nota]');
						$this->form_validation->set_rules('tanggal','Tanggal','trim|required');
						
						$no = 0;
						foreach($_POST['kode_obat'] as $d)
						{
							if( ! empty($d))
							{
								$this->form_validation->set_rules('kode_obat['.$no.']','Kode obat #'.($no + 1), 'trim|required|max_length[40]|callback_cek_kode_obat[kode_obat['.$no.']]');
								$this->form_validation->set_rules('jumlah_beli['.$no.']','Qty #'.($no + 1), 'trim|numeric|required|callback_cek_nol[jumlah_beli['.$no.']]');
							}

							$no++;
						}
						
						$this->form_validation->set_rules('cash','Total Bayar', 'trim|numeric|required|max_length[17]');
						$this->form_validation->set_rules('catatan','Catatan', 'trim|max_length[1000]');

						$this->form_validation->set_message('required', '%s harus diisi');
						$this->form_validation->set_message('cek_kode_obat', '%s tidak ditemukan');
						$this->form_validation->set_message('cek_nota', '%s sudah ada');
						$this->form_validation->set_message('cek_nol', '%s tidak boleh nol');
						$this->form_validation->set_message('alpha_numeric', '%s Harus huruf / angka !');

						if($this->form_validation->run() == TRUE)
						{
							$nomor_nota 	= $this->input->post('nomor_nota');
							$tanggal		= $this->input->post('tanggal');
							$id_kasir		= $this->input->post('id_kasir');
							$id_pasien	= $this->input->post('id_pasien');
							$bayar			= $this->input->post('cash');
							$grand_total	= $this->input->post('grand_total');
							$catatan		= $this->clean_tag_input($this->input->post('catatan'));

							if($bayar < $grand_total)
							{
								$this->query_error("Cash Kurang");
							}
							else
							{
								$this->load->model('m_penjualan_master');
								$master = $this->m_penjualan_master->insert_master($nomor_nota, $tanggal, $id_kasir, $id_pasien, $bayar, $grand_total, $catatan);
								if($master)
								{
									$id_master 	= $this->m_penjualan_master->get_id($nomor_nota)->row()->id_penjualan_m;
									$inserted	= 0;

									$this->load->model('m_penjualan_detail');
									$this->load->model('m_obat');
									$this->load->model('m_kategori_obat');

									$no_array	= 0;
									foreach($_POST['kode_obat'] as $k)
									{
										if( ! empty($k))
										{
											$kode_obat 	= $_POST['kode_obat'][$no_array];
											$jumlah_beli 	= $_POST['jumlah_beli'][$no_array];

											$harga_satuan 	= $_POST['harga_satuan'][$no_array];
											
											$sub_total 		= $_POST['sub_total'][$no_array];
											$id_obat		= $this->m_obat->get_id($kode_obat)->row()->kode_obat;
											
											$insert_detail	= $this->m_penjualan_detail->insert_detail($id_master, $id_obat, $jumlah_beli, $harga_satuan, $sub_total);
											if($insert_detail)
											{
												$this->m_obat->update_stok($id_obat, $jumlah_beli);
												$inserted++;
											}
										}

										$no_array++;
									}

									if($inserted > 0)
									{
										echo json_encode(array('status' => 1, 'pesan' => "Transaksi berhasil disimpan !"));
									}
									else
									{
										$this->query_error();
									}
								}
								else
								{
									$this->query_error();
								}
							}
						}
						else
						{
							echo json_encode(array('status' => 0, 'pesan' => validation_errors("<font color='red'>- ","</font><br />")));
						}
					}
					else
					{
						$this->query_error("Harap masukan minimal 1 kode obat !");
					}
				}
				else
				{
					$this->query_error("Harap masukan minimal 1 kode obat !");
				}
			}
			else
			{
				$this->load->model('m_user');
				$this->load->model('m_pasien');

				$dt['kasirnya'] = $this->m_user->list_kasir();
				$dt['pasien']= $this->m_pasien->get_all();
				$this->load->view('pembelian/v_pembelian', $dt);
			}
		}
	}

	public function cek_nota($nota)
	{
		$this->load->model('m_penjualan_master');
		$cek = $this->m_penjualan_master->cek_nota_validasi($nota);

		if($cek->num_rows() > 0)
		{
			return FALSE;
		}
		return TRUE;
	}

	public function transaksi_cetak()
	{
		$nomor_nota 	= $this->input->get('nomor_nota');
		$tanggal		= $this->input->get('tanggal');
		$id_kasir		= $this->input->get('id_kasir');
		$id_pasien	= $this->input->get('id_pasien');
		$cash			= $this->input->get('cash');
		$catatan		= $this->input->get('catatan');
		$grand_total	= $this->input->get('grand_total');

		$this->load->model('m_user');
		$kasir = $this->m_user->get_baris($id_kasir)->row()->nama;
		
		$this->load->model('m_pasien');
		$pasien = 'umum';
		if( ! empty($id_pasien))
		{
			$pasien = $this->m_pasien->get_baris($id_pasien)->row()->nama;
		}

		$this->load->library('cfpdf');		
		$pdf = new FPDF('P','mm','A5');
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(0, 2, 'Dialka Guna Medika p.t', 0, 0, 'L'); 
		$pdf->SetFont('Arial','',8);
		
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Cell(50, 3, 'Ruko Pesona Indah No.10', 0, 0, 'L'); 
		$pdf->Cell(85, 3, 'Bandung,', 0, 0, 'C'); 
		$pdf->Cell(0, 3, date('d-M-Y H:i:s', strtotime($tanggal)), 0, 0, 'R');
		$pdf->Ln();
		$pdf->Cell(50, 3, 'Jln.Kulon Bandung', 0, 0, 'L'); 
		$pdf->Cell(55, 3, 'Kepada Yth:', 0, 0, 'C'); 
		$pdf->Cell(-26, 3, 'Ap.Larini', 0, 0, 'C'); 
		$pdf->Ln();
		$pdf->Cell(50, 3, 'Ixin PBF No : HK', 0, 0, 'L'); 
		$pdf->Cell(115, 3, 'Jln.Cigendong No.4 Ujung Berung', 0, 0, 'C'); 
		
		$pdf->Ln();
		$pdf->Cell(50, 3, 'NPWP: 900.287.635.8-429.000', 0, 0, 'L'); 
		$pdf->Cell(84, 3, 'Bandung', 0, 0, 'C'); 
		
		$pdf->Ln();
		$pdf->Cell(0, 3, 'Telp.(022)63739198', 0, 0, 'L'); 
		
			$pdf->Ln();
			$pdf->Ln();
		$pdf->SetFont('Arial','',10);
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Cell(25, 4, 'No Faktur', 0, 0, 'L'); 
		$pdf->Cell(85, 4, $nomor_nota, 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(130, 5, '-----------------------------------------------------------------------------------------------------------', 0, 0, 'L');
		$pdf->Ln();
		
		$pdf->Cell(25, 5, 'Kode', 0, 0, 'L');
		$pdf->Cell(40, 5, 'Nama obat', 0, 0, 'L');
		$pdf->Cell(25, 5, 'Harga', 0, 0, 'L');
		$pdf->Cell(15, 5, 'Qty', 0, 0, 'L');
		$pdf->Cell(25, 5, 'Subtotal', 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(130, 5, '-----------------------------------------------------------------------------------------------------------', 0, 0, 'L');
		$pdf->Ln();

		$this->load->model('m_obat');
		$this->load->helper('text');

		$no = 0;
		foreach($_GET['kode_obat'] as $kd)
		{
			if( ! empty($kd))
			{
				$nama_obat = $this->m_obat->get_id($kd)->row()->nama_obat;
				$nama_obat = character_limiter($nama_obat, 20, '..');

				$pdf->Cell(25, 5, $kd, 0, 0, 'L');
				$pdf->Cell(40, 5, $nama_obat, 0, 0, 'L');
				$pdf->Cell(25, 5, str_replace(',', '.', number_format($_GET['harga_satuan'][$no])), 0, 0, 'L');
				$pdf->Cell(15, 5, $_GET['jumlah_beli'][$no], 0, 0, 'L');
				$pdf->Cell(25, 5, str_replace(',', '.', number_format($_GET['sub_total'][$no])), 0, 0, 'L');
				$pdf->Ln();

				$no++;
			}
		}

		$pdf->Cell(130, 5, '-----------------------------------------------------------------------------------------------------------', 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(105, 5, 'Total Bayar', 0, 0, 'R');
		$pdf->Cell(25, 5, str_replace(',', '.', number_format($grand_total)), 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(105, 5, 'Cash', 0, 0, 'R');
		$pdf->Cell(25, 5, str_replace(',', '.', number_format($cash)), 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(105, 5, 'Kembali', 0, 0, 'R');
		$pdf->Cell(25, 5, str_replace(',', '.', number_format(($cash - $grand_total))), 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(130, 5, '-----------------------------------------------------------------------------------------------------------', 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(25, 5, 'Catatan : ', 0, 0, 'L');
		$pdf->Ln();
		$pdf->Cell(130, 5, (($catatan == '') ? 'Tidak Ada' : $catatan), 0, 0, 'L');
		$pdf->Ln();

		
		$pdf->Cell(130, 5, '-----------------------------------------------------------------------------------------------------------', 0, 0, 'L');
		$pdf->Ln();
						$pdf->Ln();
						$pdf->Cell(12, 5, "", 0, 0, 'L');
						$pdf->Cell(30, 5, 'Penerima', 0, 0, 'C');
														$pdf->Cell(12, 5, "", 0, 0, 'R');
														$pdf->Cell(20, 5, "", 0, 0, 'C');
														
												$pdf->Cell(12, 5, "", 0, 0, 'L');
							$pdf->Cell(30, 5,'Hormat Kami', 0, 0, 'C');
														$pdf->Cell(12, 5, "", 0, 0, 'R');
								$pdf->Ln();
								$pdf->Ln();
								$pdf->Ln();
								$pdf->Ln();
								$pdf->Ln();
								
								$pdf->Cell(12, 5, "(", 0, 0, 'L');
		$pdf->Cell(30, 5, $pasien, 0, 0, 'C');
										$pdf->Cell(12, 5, ")", 0, 0, 'R');
										$pdf->Cell(20, 5, "", 0, 0, 'C');
										
								$pdf->Cell(12, 5, "(", 0, 0, 'L');
			$pdf->Cell(30, 5,$kasir, 0, 0, 'C');
										$pdf->Cell(12, 5, ")", 0, 0, 'R');
		
		$pdf->Output();
	}

	public function ajax_pasien()
	{
		if($this->input->is_ajax_request())
		{
			$id_pasien = $this->input->post('id_pasien');
			$this->load->model('m_pasien');

			$data = $this->m_pasien->get_baris($id_pasien)->row();
			$json['telp']			= ( ! empty($data->telp)) ? $data->telp : "<small><i>Tidak ada</i></small>";
			$json['alamat']			= ( ! empty($data->alamat)) ? preg_replace("/\r\n|\r|\n/",'<br />', $data->alamat) : "<small><i>Tidak ada</i></small>";
			$json['info_tambahan']	= ( ! empty($data->info_tambahan)) ? preg_replace("/\r\n|\r|\n/",'<br />', $data->info_tambahan) : "<small><i>Tidak ada</i></small>";
			echo json_encode($json);
		}
	}

	public function tambah()
	{
		$level = $this->session->userdata('ap_level');
		if($level !== 'admin')
		{
			exit();
		}
		else
		{
						if($_POST)
			{
				$this->load->library('form_validation');
					$this->form_validation->set_rules('nama','Nama','trim|required|max_length[60]');
					$this->form_validation->set_rules('id_kategori_obat','Kategori','trim|required');
					$this->form_validation->set_rules('id_golongan_obat','Golongan','trim|required');
					$this->form_validation->set_rules('id_satuan_obat','Satuan','trim|required');
					
					$this->form_validation->set_rules('stok','Stok','trim|required|max_length[10]');
					$this->form_validation->set_rules('tgl_pembuatan','Tanggal Pembuatan','trim|required');
					$this->form_validation->set_rules('tgl_kadaluarsa','Tanggal Kadaluarsa','trim|required');
					$this->form_validation->set_rules('harga','Harga','trim|required|numeric|min_length[4]|max_length[10]|callback_cek_titik[harga]');
					$this->form_validation->set_rules('keterangan','Keterangan','trim|max_length[2000]');
				
				$this->form_validation->set_message('required','%s harus diisi !');
				$this->form_validation->set_message('numeric','%s harus angka !');
				$this->form_validation->set_message('exist_kode','%s sudah ada di database, pilih kode lain yang unik !');
				$this->form_validation->set_message('cek_titik','%s harus angka, tidak boleh ada titik !');
				if($this->form_validation->run() == TRUE)
				{
					$this->load->model('m_barang');
					
					$nama 	= $this->input->post('nama');
					$id_kategori_obat 	= $this->input->post('id_kategori_obat');
					$id_golongan_obat		= $this->input->post('id_golongan_obat');
					$id_satuan_obat		= $this->input->post('id_satuan_obat');
					$stok = $this->input->post('stok');
					$tanggal_pembuatan	= $this->input->post('tgl_pembuatan');
					$tanggal_kadaluarsa		= $this->input->post('tgl_kadaluarsa');
					$harga = $this->input->post('harga');
					$keterangan = $this->input->post('keterangan');
					
						$insert = $this->m_barang->tambah_baru($nama, $id_kategori_obat, $id_golongan_obat,$id_satuan_obat,$stok ,$tanggal_pembuatan, $tanggal_kadaluarsa, $harga, $keterangan);

					if($insert > 0)
					{
						echo json_encode(array(
							'status' => 1,
							'pesan' => "<i class='fa fa-check' style='color:green;'></i> Data obat berhasil dismpan."
						));
					}
					else
					{
						$this->query_error("Oops, terjadi kesalahan, coba lagi !");
					}
				}
				else
				{
					$this->input_error();
				}
			}
		
			else
			{
				$this->load->model('m_kategori_obat');
				$this->load->model('m_golongan_obat');
				$this->load->model('m_satuan_obat');
				
				$dt['kategori'] = $this->m_kategori_obat->get_all();
				$dt['golongan'] = $this->m_golongan_obat->get_gol();
				$dt['satuan'] = $this->m_satuan_obat->get_sat();
				
				$this->load->view('pembelian/view_p', $dt);
			}
}
	}
	public function ajax_cek_kode()
	{
		if($this->input->is_ajax_request())
		{
			$kode = $this->input->post('kodenya');
			$this->load->model('m_barang');

			$cek_kode = $this->m_obat->cek_kode($kode);
			if($cek_kode->num_rows() > 0)
			{
				echo json_encode(array(
					'status' => 0,
					'pesan' => "<font color='red'>Kode sudah ada</font>"
				));
			}
			else
			{
				echo json_encode(array(
					'status' => 1,
					'pesan' => ''
				));
			}
		}
	}

	public function exist_kode($kode)
	{
		$this->load->model('m_obat');
		$cek_kode = $this->m_barang->cek_kode($kode);

		if($cek_kode->num_rows() > 0)
		{
			return FALSE;
		}
		return TRUE;
	}

	public function cek_titik($angka)
	{
		$pecah = explode('.', $angka);
		if(count($pecah) > 1){
			return FALSE;
		}
		return TRUE;
	}

}