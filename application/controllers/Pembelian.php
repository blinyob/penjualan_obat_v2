<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ------------------------------------------------------------------------
 * CLASS NAME : pembelian
 * ------------------------------------------------------------------------
 *
 * @author     Muhammad Akbar <muslim.politekniktelkom@gmail.com>
 * @copyright  2016
 * @license    http://aplikasiphp.net
 *
 */

class Pembelian extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('ap_level') == 'inventory'){
			redirect();
		}
	}

	public function index()
	{
		$this->transaksi_pembelian();
	}

	public function transaksi_pembelian()
	{
		$level = $this->session->userdata('ap_level');
		if($level == 'admin' OR $level == 'kasir')
		{
			if($_POST)
			{
				if( ! empty($_POST['kode_obat']))
				{
					$total = 0;
					foreach($_POST['kode_obat'] as $k)
					{
						if( ! empty($k)){ $total++; }
					}

					if($total > 0)
					{
						$this->load->library('form_validation');
						$this->form_validation->set_rules('nomor_nota','Nomor Nota','trim|required|max_length[40]|alpha_numeric|callback_cek_nota[nomor_nota]');
						$this->form_validation->set_rules('tanggal','Tanggal','trim|required');
						
						$no = 0;
						foreach($_POST['kode_obat'] as $d)
						{
							if( ! empty($d))
							{
								$this->form_validation->set_rules('kode_obat['.$no.']','Kode obat #'.($no + 1), 'trim|required|max_length[40]|callback_cek_kode_obat[kode_obat['.$no.']]');
								$this->form_validation->set_rules('jumlah_beli['.$no.']','Qty #'.($no + 1), 'trim|numeric|required|callback_cek_nol[jumlah_beli['.$no.']]');
							}

							$no++;
						}
						
						$this->form_validation->set_rules('cash','Total Bayar', 'trim|numeric|required|max_length[17]');
						$this->form_validation->set_rules('catatan','Catatan', 'trim|max_length[1000]');

						$this->form_validation->set_message('required', '%s harus diisi');
						$this->form_validation->set_message('cek_kode_obat', '%s tidak ditemukan');
						$this->form_validation->set_message('cek_nota', '%s sudah ada');
						$this->form_validation->set_message('cek_nol', '%s tidak boleh nol');
						$this->form_validation->set_message('alpha_numeric', '%s Harus huruf / angka !');

						if($this->form_validation->run() == TRUE)
						{
							$nomor_nota 	= $this->input->post('nomor_nota');
							$tanggal		= $this->input->post('tanggal');
							$id_kasir		= $this->input->post('id_kasir');
							$id_distributor		= $this->input->post('id_distributor');
							$bayar			= $this->input->post('cash');
							$grand_total	= $this->input->post('grand_total');
							$catatan		= $this->clean_tag_input($this->input->post('catatan'));

							if($bayar < $grand_total)
							{
								$this->query_error("Cash Kurang");
							}
							else
							{
								$this->load->model('m_pembelian_master');
								$master = $this->m_pembelian_master->insert_master($nomor_nota, $tanggal, $id_kasir, $id_distributor, $bayar, $grand_total, $catatan);
								if($master)
								{
									$id_master 	= $this->m_pembelian_master->get_id($nomor_nota)->row()->id_pembelian_m;
									$inserted	= 0;

									$this->load->model('m_pembelian_detail');
									$this->load->model('m_obat');
									$this->load->model('m_kategori_obat');

									$no_array	= 0;
									foreach($_POST['kode_obat'] as $k)
									{
										if( ! empty($k))
										{
											$kode_obat 	= $_POST['kode_obat'][$no_array];
											$jumlah_beli 	= $_POST['jumlah_beli'][$no_array];

											$harga_satuan 	= $_POST['harga_satuan'][$no_array];
											
											$sub_total 		= $_POST['sub_total'][$no_array];
											$id_obat		= $this->m_obat->get_id($kode_obat)->row()->kode_obat;
											
											$insert_detail	= $this->m_pembelian_detail->insert_detail($id_master, $id_obat, $jumlah_beli, $harga_satuan, $sub_total);
											if($insert_detail)
											{
												$this->m_obat->update_stok($id_obat, $jumlah_beli);
												$inserted++;
											}
										}

										$no_array++;
									}

									if($inserted > 0)
									{
										echo json_encode(array('status' => 1, 'pesan' => "Transaksi berhasil disimpan !"));
									}
									else
									{
										$this->query_error();
									}
								}
								else
								{
									$this->query_error();
								}
							}
						}
						else
						{
							echo json_encode(array('status' => 0, 'pesan' => validation_errors("<font color='red'>- ","</font><br />")));
						}
					}
					else
					{
						$this->query_error("Harap masukan minimal 1 kode obat !");
					}
				}
				else
				{
					$this->query_error("Harap masukan minimal 1 kode obat !");
				}
			}
			else
			{
				$this->load->model('m_user');
				$this->load->model('m_distributor');

				$dt['kasirnya'] = $this->m_user->list_kasir();
				$dt['distributor']= $this->m_distributor->get_dis();
				$this->load->view('pembelian/transaksi_pembelian', $dt);
			}
		}
	}

	public function cek_nota($nota)
	{
		$this->load->model('m_pembelian_master');
		$cek = $this->m_pembelian_master->cek_nota_validasi($nota);

		if($cek->num_rows() > 0)
		{
			return FALSE;
		}
		return TRUE;
	}

	public function transaksi_cetak()
	{
		$nomor_nota 	= $this->input->get('nomor_nota');
		$tanggal		= $this->input->get('tanggal');
		$id_kasir		= $this->input->get('id_kasir');
		$id_distributor	= $this->input->get('id_distributor');
		$cash			= $this->input->get('cash');
		$catatan		= $this->input->get('catatan');
		$grand_total	= $this->input->get('grand_total');

		$this->load->model('m_user');
		$kasir = $this->m_user->get_baris($id_kasir)->row()->nama;
		
		$this->load->model('m_distributor');
		$distributor = 'umum';
		if( ! empty($id_distributor))
		{
			$distributor = $this->m_distributor->get_baris($id_distributor)->row()->nama;
		}

		$this->load->library('cfpdf');		
		$pdf = new FPDF('P','mm','A5');
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(0, 2, 'Dialka Guna Medika p.t', 0, 0, 'L'); 
		$pdf->SetFont('Arial','',8);
		
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Cell(50, 3, 'Ruko Pesona Indah No.10', 0, 0, 'L'); 
		$pdf->Cell(85, 3, 'Bandung,', 0, 0, 'C'); 
		$pdf->Cell(0, 3, date('d-M-Y H:i:s', strtotime($tanggal)), 0, 0, 'R');
		$pdf->Ln();
		$pdf->Cell(50, 3, 'Jln.Kulon Bandung', 0, 0, 'L'); 
		$pdf->Cell(55, 3, 'Kepada Yth:', 0, 0, 'C'); 
		$pdf->Cell(-26, 3, 'Ap.Larini', 0, 0, 'C'); 
		$pdf->Ln();
		$pdf->Cell(50, 3, 'Ixin PBF No : HK', 0, 0, 'L'); 
		$pdf->Cell(115, 3, 'Jln.Cigendong No.4 Ujung Berung', 0, 0, 'C'); 
		
		$pdf->Ln();
		$pdf->Cell(50, 3, 'NPWP: 900.287.635.8-429.000', 0, 0, 'L'); 
		$pdf->Cell(84, 3, 'Bandung', 0, 0, 'C'); 
		
		$pdf->Ln();
		$pdf->Cell(0, 3, 'Telp.(022)63739198', 0, 0, 'L'); 
		
			$pdf->Ln();
			$pdf->Ln();
		$pdf->SetFont('Arial','',10);
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Cell(25, 4, 'No Faktur', 0, 0, 'L'); 
		$pdf->Cell(85, 4, $nomor_nota, 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(130, 5, '-----------------------------------------------------------------------------------------------------------', 0, 0, 'L');
		$pdf->Ln();
		
		$pdf->Cell(25, 5, 'Kode', 0, 0, 'L');
		$pdf->Cell(40, 5, 'Nama obat', 0, 0, 'L');
		$pdf->Cell(25, 5, 'Harga', 0, 0, 'L');
		$pdf->Cell(15, 5, 'Qty', 0, 0, 'L');
		$pdf->Cell(25, 5, 'Subtotal', 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(130, 5, '-----------------------------------------------------------------------------------------------------------', 0, 0, 'L');
		$pdf->Ln();

		$this->load->model('m_obat');
		$this->load->helper('text');

		$no = 0;
		foreach($_GET['kode_obat'] as $kd)
		{
			if( ! empty($kd))
			{
				$nama_obat = $this->m_obat->get_id($kd)->row()->nama_obat;
				$nama_obat = character_limiter($nama_obat, 20, '..');

				$pdf->Cell(25, 5, $kd, 0, 0, 'L');
				$pdf->Cell(40, 5, $nama_obat, 0, 0, 'L');
				$pdf->Cell(25, 5, str_replace(',', '.', number_format($_GET['harga_satuan'][$no])), 0, 0, 'L');
				$pdf->Cell(15, 5, $_GET['jumlah_beli'][$no], 0, 0, 'L');
				$pdf->Cell(25, 5, str_replace(',', '.', number_format($_GET['sub_total'][$no])), 0, 0, 'L');
				$pdf->Ln();

				$no++;
			}
		}

		$pdf->Cell(130, 5, '-----------------------------------------------------------------------------------------------------------', 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(105, 5, 'Total Bayar', 0, 0, 'R');
		$pdf->Cell(25, 5, str_replace(',', '.', number_format($grand_total)), 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(105, 5, 'Cash', 0, 0, 'R');
		$pdf->Cell(25, 5, str_replace(',', '.', number_format($cash)), 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(105, 5, 'Kembali', 0, 0, 'R');
		$pdf->Cell(25, 5, str_replace(',', '.', number_format(($cash - $grand_total))), 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(130, 5, '-----------------------------------------------------------------------------------------------------------', 0, 0, 'L');
		$pdf->Ln();

		$pdf->Cell(25, 5, 'Catatan : ', 0, 0, 'L');
		$pdf->Ln();
		$pdf->Cell(130, 5, (($catatan == '') ? 'Tidak Ada' : $catatan), 0, 0, 'L');
		$pdf->Ln();

		
		$pdf->Cell(130, 5, '-----------------------------------------------------------------------------------------------------------', 0, 0, 'L');
		$pdf->Ln();
						$pdf->Ln();
						$pdf->Cell(12, 5, "", 0, 0, 'L');
						$pdf->Cell(30, 5, 'Penerima', 0, 0, 'C');
														$pdf->Cell(12, 5, "", 0, 0, 'R');
														$pdf->Cell(20, 5, "", 0, 0, 'C');
														
												$pdf->Cell(12, 5, "", 0, 0, 'L');
							$pdf->Cell(30, 5,'Hormat Kami', 0, 0, 'C');
														$pdf->Cell(12, 5, "", 0, 0, 'R');
								$pdf->Ln();
								$pdf->Ln();
								$pdf->Ln();
								$pdf->Ln();
								$pdf->Ln();
								
								$pdf->Cell(12, 5, "(", 0, 0, 'L');
		$pdf->Cell(30, 5, $distributor, 0, 0, 'C');
										$pdf->Cell(12, 5, ")", 0, 0, 'R');
										$pdf->Cell(20, 5, "", 0, 0, 'C');
										
								$pdf->Cell(12, 5, "(", 0, 0, 'L');
			$pdf->Cell(30, 5,$kasir, 0, 0, 'C');
										$pdf->Cell(12, 5, ")", 0, 0, 'R');
		
		$pdf->Output();
	}

	public function ajax_distributor()
	{
		if($this->input->is_ajax_request())
		{
			$id_distributor = $this->input->post('id_distributor');
			$this->load->model('m_distributor');

			$data = $this->m_distributor->get_baris($id_distributor)->row();
			$json['telp']			= ( ! empty($data->telp)) ? $data->telp : "<small><i>Tidak ada</i></small>";
			$json['alamat']			= ( ! empty($data->alamat)) ? preg_replace("/\r\n|\r|\n/",'<br />', $data->alamat) : "<small><i>Tidak ada</i></small>";
			$json['info_tambahan']	= ( ! empty($data->info_tambahan)) ? preg_replace("/\r\n|\r|\n/",'<br />', $data->info_tambahan) : "<small><i>Tidak ada</i></small>";
			echo json_encode($json);
		}
	}

	public function ajax_kode()
	{
		if($this->input->is_ajax_request())
		{
			$keyword 	= $this->input->post('keyword');
			$registered	= $this->input->post('registered');

			$this->load->model('m_obat');

			$obat = $this->m_obat->cari_kode($keyword, $registered);

			if($obat->num_rows() > 0)
			{
				$json['status'] 	= 1;
				$json['datanya'] 	= "<ul id='daftar-autocomplete'>";
				foreach($obat->result() as $b)
				{
					$json['datanya'] .= "
						<li>
							<b>Kode</b> : 
							<span id='kodenya'>".$b->kode_obat."</span> <br />
							<span id='obatnya'>".$b->nama_obat."</span>
							<span id='kategorinya' style='display:none;'>".$b->kategori."</span>
							<span id='total_stoknya' style='display:none;'>".$b->total_stok."</span>
							<span id='harganya' style='display:none;'>".$b->harga."</span>
						</li>
					";
				}
				$json['datanya'] .= "</ul>";
			}
			else
			{
				$json['status'] 	= 0;
			}

			echo json_encode($json);
		}
	}

	public function cek_kode_obat($kode)
	{
		$this->load->model('m_obat');
		$cek_kode = $this->m_obat->cek_kode($kode);

		if($cek_kode->num_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}

	public function cek_nol($qty)
	{
		if($qty > 0){
			return TRUE;
		}
		return FALSE;
	}

	public function history()
	{
		$level = $this->session->userdata('ap_level');
		if($level == 'admin' OR $level == 'kasir' OR $level == 'keuangan')
		{
			$this->load->view('pembelian/transaksi_history_pembelian');
		}
	}

	public function history_json()
	{
		$this->load->model('m_pembelian_master');
		$level 			= $this->session->userdata('ap_level');

		$requestData	= $_REQUEST;
		$fetch			= $this->m_pembelian_master->fetch_data_pembelian($requestData['search']['value'], $requestData['order'][0]['column'], $requestData['order'][0]['dir'], $requestData['start'], $requestData['length']);
		
		$totalData		= $fetch['totalData'];
		$totalFiltered	= $fetch['totalFiltered'];
		$query			= $fetch['query'];

		$data	= array();
		foreach($query->result_array() as $row)
		{ 
			$nestedData = array(); 

			$nestedData[]	= $row['nomor'];
			$nestedData[]	= $row['tanggal'];
			$nestedData[]	= "<a href='".site_url('pembelian/detail-transaksi/'.$row['id_pembelian_m'])."' id='LihatDetailTransaksi'><i class='fa fa-file-text-o fa-fw'></i> ".$row['nomor_nota']."</a>";
			$nestedData[]	= $row['grand_total'];
			$nestedData[]	= $row['nama_distributor'];
			$nestedData[]	= preg_replace("/\r\n|\r|\n/",'<br />', $row['keterangan']);
			$nestedData[]	= $row['kasir'];
		
			if($level == 'admin' OR $level == 'keuangan')
			{
			
				$nestedData[]	= "<a href='".site_url('pembelian/hapus-transaksi/'.$row['id_pembelian_m'])."' id='HapusTransaksi'><i class='fa fa-trash-o'></i> Hapus</a>";
			}

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),  
			"recordsTotal"    => intval( $totalData ),  
			"recordsFiltered" => intval( $totalFiltered ), 
			"data"            => $data
			);

		echo json_encode($json_data);
	}

	public function detail_transaksi($id_pembelian)
	{
		if($this->input->is_ajax_request())
		{
			$this->load->model('m_pembelian_detail');
			$this->load->model('m_pembelian_master');

			$dt['detail'] = $this->m_pembelian_detail->get_detail($id_pembelian);
			$dt['master'] = $this->m_pembelian_master->get_baris($id_pembelian)->row();
			
			$this->load->view('pembelian/transaksi_pembelian_detail', $dt);
		}
	}

	public function hapus_transaksi($id_pembelian)
	{
		if($this->input->is_ajax_request())
		{
			$level 	= $this->session->userdata('ap_level');
			if($level == 'admin')
			{
				$reverse_stok = $this->input->post('reverse_stok');

				$this->load->model('m_pembelian_master');

				$nota 	= $this->m_pembelian_master->get_baris($id_pembelian)->row()->nomor_nota;
				$hapus 	= $this->m_pembelian_master->hapus_transaksi($id_pembelian, $reverse_stok);
				if($hapus)
				{
					echo json_encode(array(
						"pesan" => "<font color='green'><i class='fa fa-check'></i> Transaksi <b>".$nota."</b> berhasil dihapus !</font>
					"));
				}
				else
				{
					echo json_encode(array(
						"pesan" => "<font color='red'><i class='fa fa-warning'></i> Terjadi kesalahan, coba lagi !</font>
					"));
				}
			}
		}
	}

	public function distributor()
	{
		$level = $this->session->userdata('ap_level');
		if($level == 'admin' OR $level == 'kasir' OR $level == 'keuangan')
		{
			$this->load->view('pembelian/distributor_data');
		}
	}

	public function distributor_json()
	{
		$this->load->model('m_distributor');
		$level 			= $this->session->userdata('ap_level');

		$requestData	= $_REQUEST;
		$fetch			= $this->m_distributor->fetch_data_distributor($requestData['search']['value'], $requestData['order'][0]['column'], $requestData['order'][0]['dir'], $requestData['start'], $requestData['length']);
		
		$totalData		= $fetch['totalData'];
		$totalFiltered	= $fetch['totalFiltered'];
		$query			= $fetch['query'];

		$data	= array();
		foreach($query->result_array() as $row)
		{ 
			$nestedData = array(); 

			$nestedData[]	= $row['nomor'];
			$nestedData[]	= $row['nama'];
			$nestedData[]	= preg_replace("/\r\n|\r|\n/",'<br />', $row['alamat']);
			$nestedData[]	= $row['telp'];
			$nestedData[]	= preg_replace("/\r\n|\r|\n/",'<br />', $row['info_tambahan']);
			$nestedData[]	= $row['waktu_input'];
			
			if($level == 'admin' OR $level == 'kasir' OR $level == 'keuangan') 
			{
				$nestedData[]	= "<a href='".site_url('pembelian/distributor-edit/'.$row['id_distributor'])."' id='Editdistributor'><i class='fa fa-pencil'></i> Edit</a>";
			}

			if($level == 'admin') 
			{
				$nestedData[]	= "<a href='".site_url('pembelian/distributor-hapus/'.$row['id_distributor'])."' id='Hapusdistributor'><i class='fa fa-trash-o'></i> Hapus</a>";
			}

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),  
			"recordsTotal"    => intval( $totalData ),  
			"recordsFiltered" => intval( $totalFiltered ), 
			"data"            => $data
			);

		echo json_encode($json_data);
	}

	public function tambah_distributor()
	{
		$level = $this->session->userdata('ap_level');
		if($level == 'admin' OR $level == 'kasir' OR $level == 'keuangan')
		{
			if($_POST)
			{
				$this->load->library('form_validation');
				$this->form_validation->set_rules('nama','Nama','trim|required|max_length[40]');
				$this->form_validation->set_rules('alamat','Alamat','trim|required|max_length[1000]');
				$this->form_validation->set_rules('telepon','Telepon / Handphone','trim|required|numeric|max_length[40]');
				$this->form_validation->set_rules('info','Info Tambahan Lainnya','trim|max_length[1000]');

				$this->form_validation->set_message('alpha_spaces','%s harus alphabet !');
				$this->form_validation->set_message('numeric','%s harus angka !');
				$this->form_validation->set_message('required','%s harus diisi !');

				if($this->form_validation->run() == TRUE)
				{
					$this->load->model('m_distributor');
					$nama 		= $this->input->post('nama');
					$alamat 	= $this->clean_tag_input($this->input->post('alamat'));
					$telepon 	= $this->input->post('telepon');
					$info 		= $this->clean_tag_input($this->input->post('info'));

					$unique		= time().$this->session->userdata('ap_id_user');
					$insert 	= $this->m_distributor->tambah_distributor($nama, $alamat, $telepon, $info, $unique);
					if($insert)
					{
						$id_distributor = $this->m_distributor->get_dari_kode($unique)->row()->id_distributor;
						echo json_encode(array(
							'status' => 1,
							'pesan' => "<div class='alert alert-success'><i class='fa fa-check'></i> <b>".$nama."</b> Berhasil ditambahkan sebagai distributor.</div>",
							'id_distributor' => $id_distributor,
							'nama' => $nama,
							'alamat' => preg_replace("/\r\n|\r|\n/",'<br />', $alamat),
							'telepon' => $telepon,
							'info' => (empty($info)) ? "<small><i>Tidak ada</i></small>" : preg_replace("/\r\n|\r|\n/",'<br />', $info)						
						));
					}
					else
					{
						$this->query_error();
					}
				}
				else
				{
					$this->input_error();
				}
			}
			else
			{
				$this->load->view('distributor/distributor_tambah');
			}
		}
	}

	public function distributor_edit($id_distributor = NULL)
	{
		if( ! empty($id_distributor))
		{
			$level = $this->session->userdata('ap_level');
			if($level == 'admin' OR $level == 'kasir' OR $level == 'keuangan')
			{
				if($this->input->is_ajax_request())
				{
					$this->load->model('m_distributor');
					
					if($_POST)
					{
						$this->load->library('form_validation');
						$this->form_validation->set_rules('nama','Nama','trim|required|max_length[40]');
						$this->form_validation->set_rules('alamat','Alamat','trim|required|max_length[1000]');
						$this->form_validation->set_rules('telepon','Telepon / Handphone','trim|required|numeric|max_length[40]');
						$this->form_validation->set_rules('info','Info Tambahan Lainnya','trim|max_length[1000]');

						$this->form_validation->set_message('alpha_spaces','%s harus alphabet !');
						$this->form_validation->set_message('numeric','%s harus angka !');
						$this->form_validation->set_message('required','%s harus diisi !');

						if($this->form_validation->run() == TRUE)
						{
							$nama 		= $this->input->post('nama');
							$alamat 	= $this->clean_tag_input($this->input->post('alamat'));
							$telepon 	= $this->input->post('telepon');
							$info 		= $this->clean_tag_input($this->input->post('info'));

							$update 	= $this->m_distributor->update_distributor($id_distributor, $nama, $alamat, $telepon, $info);
							if($update)
							{
								echo json_encode(array(
									'status' => 1,
									'pesan' => "<div class='alert alert-success'><i class='fa fa-check'></i> Data berhasil diupdate.</div>"
								));
							}
							else
							{
								$this->query_error();
							}
						}
						else
						{
							$this->input_error();
						}
					}
					else
					{
						$dt['distributor'] = $this->m_distributor->get_baris($id_distributor)->row();
						$this->load->view('pembelian/distributor_edit', $dt);
					}
				}
			}
		}
	}

	public function distributor_hapus($id_distributor)
	{
		$level = $this->session->userdata('ap_level');
		if($level == 'admin')
		{
			if($this->input->is_ajax_request())
			{
				$this->load->model('m_distributor');
				$hapus = $this->m_distributor->hapus_distributor($id_distributor);
				if($hapus)
				{
					echo json_encode(array(
						"pesan" => "<font color='green'><i class='fa fa-check'></i> Data berhasil dihapus !</font>
					"));
				}
				else
				{
					echo json_encode(array(
						"pesan" => "<font color='red'><i class='fa fa-warning'></i> Terjadi kesalahan, coba lagi !</font>
					"));
				}
			}
		}
	}
}