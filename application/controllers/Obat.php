<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ------------------------------------------------------------------------
 * CLASS NAME : obat
 * ------------------------------------------------------------------------
 *
 * @author     Muhammad Akbar <muslim.politekniktelkom@gmail.com>
 * @copyright  2016
 * @license    http://aplikasiphp.net
 *
 */

class Obat extends CI_Controller 
{

	public function index()
	{
		$this->tambah();
	}

	public function load_obat()
	{
		  $this->load->model('m_obat');
		  $data = $this->m_obat->get_obat();
		  echo json_encode($data);
	}

	public function obat_json()
	{
		$this->load->model('m_obat');
		$level 			= $this->session->userdata('ap_level');

		$requestData	= $_REQUEST;
		$fetch			= $this->m_obat->fetch_data_obat($requestData['search']['value'], $requestData['order'][0]['column'], $requestData['order'][0]['dir'], $requestData['start'], $requestData['length']);
		
		$totalData		= $fetch['totalData'];
		$totalFiltered	= $fetch['totalFiltered'];
		$query			= $fetch['query'];

		$data	= array();
		foreach($query->result_array() as $row)
		{ 
			$nestedData = array(); 

			$nestedData[]	= $row['nomor'];
			$nestedData[]	= $row['kode_obat'];
			$nestedData[]	= $row['nama_obat'];
			$nestedData[]	= $row['kategori'];
			$nestedData[]	= $row['golongan'];
			$nestedData[]	= $row['satuan'];
			$nestedData[]	= ($row['total_stok'] == 'Kosong') ? "<font color='red'><b>".$row['total_stok']."</b></font>" : $row['total_stok'];
			$nestedData[]	= $row['harga'];
			$nestedData[]	= $row['tgl_pembuatan'];
			$nestedData[]	= $row['tgl_kad'];
			
			$nestedData[]	= preg_replace("/\r\n|\r|\n/",'<br />', $row['keterangan']);

			if($level == 'admin' OR $level == 'inventory')
			{
				$nestedData[]	= "<a href='".site_url('obat/edit/'.$row['kode_obat'])."' id='Editobat'><i class='fa fa-pencil'></i> Edit</a>";
				$nestedData[]	= "<a href='".site_url('obat/hapus/'.$row['kode_obat'])."' id='Hapusobat'><i class='fa fa-trash-o'></i> Hapus</a>";
			}

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),  
			"recordsTotal"    => intval( $totalData ),  
			"recordsFiltered" => intval( $totalFiltered ), 
			"data"            => $data
			);

		echo json_encode($json_data);
	}

	public function hapus($id_obat)
	{
		$level = $this->session->userdata('ap_level');
		if($level == 'admin' OR $level == 'inventory')
		{
			if($this->input->is_ajax_request())
			{
				$this->load->model('m_obat');
				$hapus = $this->m_obat->hapus_obat($id_obat);
				if($hapus)
				{
					echo json_encode(array(
						"pesan" => "<font color='green'><i class='fa fa-check'></i> Data berhasil dihapus !</font>
					"));
				}
				else
				{
					echo json_encode(array(
						"pesan" => "<font color='red'><i class='fa fa-warning'></i> Terjadi kesalahan, coba lagi !</font>
					"));
				}
			}
		}
	}

	public function tambah()
	{
		$level = $this->session->userdata('ap_level');
		if($level == 'admin' OR $level == 'inventory')
		{
			if($_POST)
			{
				$this->load->library('form_validation');

				$no = 0;
				foreach($_POST['kode'] as $kode)
				{
					$this->form_validation->set_rules('nama['.$no.']','Nama obat #'.($no + 1),'trim|required|max_length[60]');
					$this->form_validation->set_rules('id_kategori_obat['.$no.']','Kategori #'.($no + 1),'trim|required');
					$this->form_validation->set_rules('id_golongan_obat['.$no.']','Golongan #'.($no + 1),'trim|required');
					$this->form_validation->set_rules('id_satuan_obat['.$no.']','Satuan #'.($no + 1),'trim|required');
					
					$this->form_validation->set_rules('stok['.$no.']','Stok #'.($no + 1),'trim|required|numeric|max_length[10]|callback_cek_titik[stok['.$no.']]');
					$this->form_validation->set_rules('tgl_pembuatan['.$no.']','Tanggal Pembuatan #'.($no + 1),'trim|required');
					$this->form_validation->set_rules('tgl_kad['.$no.']','Tanggal Kadaluarsa #'.($no + 1),'trim|required');
					
					$this->form_validation->set_rules('harga['.$no.']','Harga #'.($no + 1),'trim|required|numeric|min_length[4]|max_length[10]|callback_cek_titik[harga['.$no.']]');
					$this->form_validation->set_rules('keterangan['.$no.']','Keterangan #'.($no + 1),'trim|max_length[2000]');
					$no++;
				}
				
				$this->form_validation->set_message('required','%s harus diisi !');
				$this->form_validation->set_message('numeric','%s harus angka !');
				$this->form_validation->set_message('exist_kode','%s sudah ada di database, pilih kode lain yang unik !');
				$this->form_validation->set_message('cek_titik','%s harus angka, tidak boleh ada titik !');
				$this->form_validation->set_message('alpha_numeric_spaces', '%s Harus huruf / angka !');
				$this->form_validation->set_message('alpha_numeric', '%s Harus huruf / angka !');
				if($this->form_validation->run() == TRUE)
				{
					$this->load->model('m_obat');

					$no_array = 0;
					$inserted = 0;
					foreach($_POST['kode'] as $k)
					{
						$nama 				= $_POST['nama'][$no_array];
						$id_kategori_obat	= $_POST['id_kategori_obat'][$no_array];
						$id_golongan_obat	= $_POST['id_golongan_obat'][$no_array];
						$id_satuan_obat	= $_POST['id_satuan_obat'][$no_array];
					
						$stok 				= $_POST['stok'][$no_array];
						$harga 				= $_POST['harga'][$no_array];
						$tgl_pembuatan 				= $_POST['tgl_pembuatan'][$no_array];
						$tgl_kad		= $_POST['tgl_kad'][$no_array];
						
						$keterangan 		= $this->clean_tag_input($_POST['keterangan'][$no_array]);

						$insert = $this->m_obat->tambah_baru($nama, $id_kategori_obat,$id_golongan_obat,$id_satuan_obat, $stok, $harga, $keterangan);
						if($insert){
							$inserted++;
						}
						$no_array++;
					}

					if($inserted > 0)
					{
						echo json_encode(array(
							'status' => 1,
							'pesan' => "<i class='fa fa-check' style='color:green;'></i> Data obat berhasil dismpan."
						));
					}
					else
					{
						$this->query_error("Oops, terjadi kesalahan, coba lagi !");
					}
				}
				else
				{
					$this->input_error();
				}
			}
			else
			{
				$this->load->model('m_obat');
				$this->load->model('m_kategori_obat');
				$this->load->model('m_golongan_obat');
				$this->load->model('m_satuan_obat');
				
				$dt['obat'] = $this->m_obat->get_obat();
				$dt['kategori'] = $this->m_kategori_obat->get_all();
				$dt['golongan'] = $this->m_golongan_obat->get_gol();
				$dt['satuan'] = $this->m_satuan_obat->get_sat();

				$this->load->view('obat/obat_data2', $dt);
			}
		}
		else
		{
			exit();
		}
	}

	public function ajax_cek_kode()
	{
		if($this->input->is_ajax_request())
		{
			$kode = $this->input->post('kodenya');
			$this->load->model('m_obat');

			$cek_kode = $this->m_obat->cek_kode($kode);
			if($cek_kode->num_rows() > 0)
			{
				echo json_encode(array(
					'status' => 0,
					'pesan' => "<font color='red'>Kode sudah ada</font>"
				));
			}
			else
			{
				echo json_encode(array(
					'status' => 1,
					'pesan' => ''
				));
			}
		}
	}

	public function exist_kode($kode)
	{
		$this->load->model('m_obat');
		$cek_kode = $this->m_obat->cek_kode($kode);

		if($cek_kode->num_rows() > 0)
		{
			return FALSE;
		}
		return TRUE;
	}

	public function cek_titik($angka)
	{
		$pecah = explode('.', $angka);
		if(count($pecah) > 1){
			return FALSE;
		}
		return TRUE;
	}

	public function edit_obat2()
	{
		$data = array(
			$this->input->post('table_column')	=>	$this->input->post('value')
		);

		$this->m_obat->update($data, $this->input->post('id'));
	}

	public function edit_obat()
	{
		$id= $this->input->post("id");
		$value= $this->input->post("value");
		$modul= $this->input->post("modul");
		$this->load->model('m_obat');
		$edit=$this->m_obat->update_obat2($id,$value,$modul);
		echo "{}";
		if ($edit) {
			$this->session->set_flashdata('gagal',"<font color='green'><i class='fa fa-check'></i> Data berhasil dihapus !</font>");
		}else{
			$this->session->set_flashdata('gagal',"<font color='green'><i class='fa fa-check'></i> Data berhasil dihapus !</font>");			
		}
	}

	public function edit($id_obat = NULL)
	{
		if( ! empty($id_obat))
		{
			$level = $this->session->userdata('ap_level');
			if($level == 'admin' OR $level == 'inventory')
			{
				if($this->input->is_ajax_request())
				{
					$this->load->model('m_obat');
					
					if($_POST)
					{
						$this->load->library('form_validation');


						$this->form_validation->set_rules('nama_obat','Nama obat','trim|required|max_length[60]');
						$this->form_validation->set_rules('id_kategori_obat','Kategori','trim|required');
						$this->form_validation->set_rules('id_golongan_obat','Golongan','trim|required');
						$this->form_validation->set_rules('id_satuan_obat','Satuan','trim|required');
						
						$this->form_validation->set_rules('total_stok','Stok','trim|required|numeric|max_length[10]|callback_cek_titik[total_stok]');
						$this->form_validation->set_rules('harga','Harga','trim|required|numeric|min_length[4]|max_length[10]|callback_cek_titik[harga]');
						$this->form_validation->set_rules('tgl_pembuatan','Tanggal Pembuatan','trim|required');
						$this->form_validation->set_rules('tgl_kad','Tanggal Kadaluarsa','trim|required');
					
						$this->form_validation->set_rules('keterangan','Keterangan','trim|max_length[2000]');
						
						$this->form_validation->set_message('required','%s harus diisi !');
						$this->form_validation->set_message('numeric','%s harus angka !');
						$this->form_validation->set_message('exist_kode','%s sudah ada di database, pilih kode lain yang unik !');
						$this->form_validation->set_message('cek_titik','%s harus angka, tidak boleh ada titik !');
						$this->form_validation->set_message('alpha_numeric_spaces', '%s Harus huruf / angka !');
						$this->form_validation->set_message('alpha_numeric', '%s Harus huruf / angka !');
						
						if($this->form_validation->run() == TRUE)
						{
							$nama 				= $this->input->post('nama_obat');
							$id_kategori_obat	= $this->input->post('id_kategori_obat');
							$id_golongan_obat	= $this->input->post('id_golongan_obat');
							$id_satuan_obat	= $this->input->post('id_satuan_obat');
							$stok 				= $this->input->post('total_stok');
							$harga 				= $this->input->post('harga');
							$tgl_pembuatan 				= $this->input->post('tgl_pembuatan');
							$tgl_kad		= $this->input->post('tgl_kad');
							
							$keterangan 		= $this->clean_tag_input($this->input->post('keterangan'));

							$update = $this->m_obat->update_obat($id_obat, $nama,  $id_kategori_obat,$id_golongan_obat,$id_satuan_obat, $stok, $harga, $tgl_pembuatan, $tgl_kad, $keterangan);
							if($update)
							{
								echo json_encode(array(
									'status' => 1,
									'pesan' => "<div class='alert alert-success'><i class='fa fa-check'></i> Data obat berhasil diupdate.</div>"
								));
							}
							else
							{
								$this->query_error();
							}
						}
						else
						{
							$this->input_error();
						}
					}
					else
					{
						$this->load->model('m_kategori_obat');
						$this->load->model('m_golongan_obat');
						$this->load->model('m_satuan_obat');

						$dt['obat'] 	= $this->m_obat->get_baris($id_obat)->row();
						$dt['kategori'] = $this->m_kategori_obat->get_all();
						$dt['golongan'] = $this->m_kategori_obat->get_gol();
						$dt['satuan'] = $this->m_kategori_obat->get_sat();

						$this->load->view('obat/obat_edit', $dt);
					}
				}
			}
		}
	}
	public function list_golongan()
	{
		$this->load->view('obat/golongan/golongan_data');
	}

	public function list_golongan_json()
	{
		$this->load->model('m_golongan_obat');
		$level 			= $this->session->userdata('ap_level');

		$requestData	= $_REQUEST;
		$fetch			= $this->m_golongan_obat->fetch_data_golongan($requestData['search']['value'], $requestData['order'][0]['column'], $requestData['order'][0]['dir'], $requestData['start'], $requestData['length']);
		
		$totalData		= $fetch['totalData'];
		$totalFiltered	= $fetch['totalFiltered'];
		$query			= $fetch['query'];

		$data	= array();
		foreach($query->result_array() as $row)
		{ 
			$nestedData = array(); 

			$nestedData[]	= $row['nomor'];
			$nestedData[]	= $row['golongan'];

			if($level == 'admin' OR $level == 'inventory')
			{
				$nestedData[]	= "<a href='".site_url('obat/edit-golongan/'.$row['id_golongan_obat'])."' id='Editgolongan'><i class='fa fa-pencil'></i> Edit</a>";
				$nestedData[]	= "<a href='".site_url('obat/hapus-golongan/'.$row['id_golongan_obat'])."' id='Hapusgolongan'><i class='fa fa-trash-o'></i> Hapus</a>";
			}

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),  
			"recordsTotal"    => intval( $totalData ),  
			"recordsFiltered" => intval( $totalFiltered ), 
			"data"            => $data
			);

		echo json_encode($json_data);
	}

	public function tambah_golongan()
	{
		$level = $this->session->userdata('ap_level');
		if($level == 'admin' OR $level == 'inventory')
		{
			if($_POST)
			{
				$this->load->library('form_validation');
				$this->form_validation->set_rules('golongan','golongan','trim|required|max_length[40]|alpha_numeric_spaces');				
				$this->form_validation->set_message('required','%s harus diisi !');
				$this->form_validation->set_message('alpha_numeric_spaces', '%s Harus huruf / angka !');

				if($this->form_validation->run() == TRUE)
				{
					$this->load->model('m_golongan_obat');
					$golongan 	= $this->input->post('golongan');
					$insert = $this->m_golongan_obat->tambah_golongan($golongan);
					if($insert)
					{
						echo json_encode(array(
							'status' => 1,
							'pesan' => "<div class='alert alert-success'><i class='fa fa-check'></i> <b>".$golongan."</b> berhasil ditambahkan.</div>"
						));
					}
					else
					{
						$this->query_error();
					}
				}
				else
				{
					$this->input_error();
				}
			}
			else
			{
				$this->load->view('obat/golongan/golongan_tambah');
			}
		}
	}

	public function hapus_golongan($id_golongan_obat)
	{
		$level = $this->session->userdata('ap_level');
		if($level == 'admin' OR $level == 'inventory')
		{
			if($this->input->is_ajax_request())
			{
				$this->load->model('m_golongan_obat');
				$hapus = $this->m_golongan_obat->hapus_golongan($id_golongan_obat);
				if($hapus)
				{
					echo json_encode(array(
						"pesan" => "<font color='green'><i class='fa fa-check'></i> Data berhasil dihapus !</font>
					"));
				}
				else
				{
					echo json_encode(array(
						"pesan" => "<font color='red'><i class='fa fa-warning'></i> Terjadi kesalahan, coba lagi !</font>
					"));
				}
			}
		}
	}

	public function edit_golongan($id_golongan_obat = NULL)
	{
		if( ! empty($id_golongan_obat))
		{
			$level = $this->session->userdata('ap_level');
			if($level == 'admin' OR $level == 'inventory')
			{
				if($this->input->is_ajax_request())
				{
					$this->load->model('m_golongan_obat');
					
					if($_POST)
					{
						$this->load->library('form_validation');
						$this->form_validation->set_rules('golongan','golongan','trim|required|max_length[40]|alpha_numeric_spaces');				
						$this->form_validation->set_message('required','%s harus diisi !');
						$this->form_validation->set_message('alpha_numeric_spaces', '%s Harus huruf / angka !');

						if($this->form_validation->run() == TRUE)
						{
							$golongan 	= $this->input->post('golongan');
							$insert = $this->m_golongan_obat->update_golongan($id_golongan_obat, $golongan);
							if($insert)
							{
								echo json_encode(array(
									'status' => 1,
									'pesan' => "<div class='alert alert-success'><i class='fa fa-check'></i> Data berhasil diupdate.</div>"
								));
							}
							else
							{
								$this->query_error();
							}
						}
						else
						{
							$this->input_error();
						}
					}
					else
					{
						$dt['golongan'] = $this->m_golongan_obat->get_baris($id_golongan_obat)->row();
						$this->load->view('obat/golongan/golongan_edit', $dt);
					}
				}
			}
		}
	}

	public function list_satuan()
	{
		$this->load->view('obat/satuan/satuan_data');
	}

	public function list_satuan_json()
	{
		$this->load->model('m_satuan_obat');
		$level 			= $this->session->userdata('ap_level');

		$requestData	= $_REQUEST;
		$fetch			= $this->m_satuan_obat->fetch_data_satuan($requestData['search']['value'], $requestData['order'][0]['column'], $requestData['order'][0]['dir'], $requestData['start'], $requestData['length']);
		
		$totalData		= $fetch['totalData'];
		$totalFiltered	= $fetch['totalFiltered'];
		$query			= $fetch['query'];

		$data	= array();
		foreach($query->result_array() as $row)
		{ 
			$nestedData = array(); 

			$nestedData[]	= $row['nomor'];
			$nestedData[]	= $row['satuan'];

			if($level == 'admin' OR $level == 'inventory')
			{
				$nestedData[]	= "<a href='".site_url('obat/edit-satuan/'.$row['id_satuan_obat'])."' id='Editsatuan'><i class='fa fa-pencil'></i> Edit</a>";
				$nestedData[]	= "<a href='".site_url('obat/hapus-satuan/'.$row['id_satuan_obat'])."' id='Hapussatuan'><i class='fa fa-trash-o'></i> Hapus</a>";
			}

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),  
			"recordsTotal"    => intval( $totalData ),  
			"recordsFiltered" => intval( $totalFiltered ), 
			"data"            => $data
			);

		echo json_encode($json_data);
	}

	public function tambah_satuan()
	{
		$level = $this->session->userdata('ap_level');
		if($level == 'admin' OR $level == 'inventory')
		{
			if($_POST)
			{
				$this->load->library('form_validation');
				$this->form_validation->set_rules('satuan','satuan','trim|required|max_length[40]|alpha_numeric_spaces');				
				$this->form_validation->set_message('required','%s harus diisi !');
				$this->form_validation->set_message('alpha_numeric_spaces', '%s Harus huruf / angka !');

				if($this->form_validation->run() == TRUE)
				{
					$this->load->model('m_satuan_obat');
					$satuan 	= $this->input->post('satuan');
					$insert = $this->m_satuan_obat->tambah_satuan($satuan);
					if($insert)
					{
						echo json_encode(array(
							'status' => 1,
							'pesan' => "<div class='alert alert-success'><i class='fa fa-check'></i> <b>".$satuan."</b> berhasil ditambahkan.</div>"
						));
					}
					else
					{
						$this->query_error();
					}
				}
				else
				{
					$this->input_error();
				}
			}
			else
			{
				$this->load->view('obat/satuan/satuan_tambah');
			}
		}
	}

	public function hapus_satuan($id_satuan_obat)
	{
		$level = $this->session->userdata('ap_level');
		if($level == 'admin' OR $level == 'inventory')
		{
			if($this->input->is_ajax_request())
			{
				$this->load->model('m_satuan_obat');
				$hapus = $this->m_satuan_obat->hapus_satuan($id_satuan_obat);
				if($hapus)
				{
					echo json_encode(array(
						"pesan" => "<font color='green'><i class='fa fa-check'></i> Data berhasil dihapus !</font>
					"));
				}
				else
				{
					echo json_encode(array(
						"pesan" => "<font color='red'><i class='fa fa-warning'></i> Terjadi kesalahan, coba lagi !</font>
					"));
				}
			}
		}
	}

	public function edit_satuan($id_satuan_obat = NULL)
	{
		if( ! empty($id_satuan_obat))
		{
			$level = $this->session->userdata('ap_level');
			if($level == 'admin' OR $level == 'inventory')
			{
				if($this->input->is_ajax_request())
				{
					$this->load->model('m_satuan_obat');
					
					if($_POST)
					{
						$this->load->library('form_validation');
						$this->form_validation->set_rules('satuan','satuan','trim|required|max_length[40]|alpha_numeric_spaces');				
						$this->form_validation->set_message('required','%s harus diisi !');
						$this->form_validation->set_message('alpha_numeric_spaces', '%s Harus huruf / angka !');

						if($this->form_validation->run() == TRUE)
						{
							$satuan 	= $this->input->post('satuan');
							$insert = $this->m_satuan_obat->update_satuan($id_satuan_obat, $satuan);
							if($insert)
							{
								echo json_encode(array(
									'status' => 1,
									'pesan' => "<div class='alert alert-success'><i class='fa fa-check'></i> Data berhasil diupdate.</div>"
								));
							}
							else
							{
								$this->query_error();
							}
						}
						else
						{
							$this->input_error();
						}
					}
					else
					{
						$dt['satuan'] = $this->m_satuan_obat->get_baris($id_satuan_obat)->row();
						$this->load->view('obat/satuan/satuan_edit', $dt);
					}
				}
			}
		}
	}

	public function list_kategori()
	{
		$this->load->view('obat/kategori/kategori_data');
	}

	public function list_kategori_json()
	{
		$this->load->model('m_kategori_obat');
		$level 			= $this->session->userdata('ap_level');

		$requestData	= $_REQUEST;
		$fetch			= $this->m_kategori_obat->fetch_data_kategori($requestData['search']['value'], $requestData['order'][0]['column'], $requestData['order'][0]['dir'], $requestData['start'], $requestData['length']);
		
		$totalData		= $fetch['totalData'];
		$totalFiltered	= $fetch['totalFiltered'];
		$query			= $fetch['query'];

		$data	= array();
		foreach($query->result_array() as $row)
		{ 
			$nestedData = array(); 

			$nestedData[]	= $row['nomor'];
			$nestedData[]	= $row['kategori'];

			if($level == 'admin' OR $level == 'inventory')
			{
				$nestedData[]	= "<a href='".site_url('obat/edit-kategori/'.$row['id_kategori_obat'])."' id='EditKategori'><i class='fa fa-pencil'></i> Edit</a>";
				$nestedData[]	= "<a href='".site_url('obat/hapus-kategori/'.$row['id_kategori_obat'])."' id='HapusKategori'><i class='fa fa-trash-o'></i> Hapus</a>";
			}

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),  
			"recordsTotal"    => intval( $totalData ),  
			"recordsFiltered" => intval( $totalFiltered ), 
			"data"            => $data
			);

		echo json_encode($json_data);
	}

	public function tambah_kategori()
	{
		$level = $this->session->userdata('ap_level');
		if($level == 'admin' OR $level == 'inventory')
		{
			if($_POST)
			{
				$this->load->library('form_validation');
				$this->form_validation->set_rules('kategori','Kategori','trim|required|max_length[40]|alpha_numeric_spaces');				
				$this->form_validation->set_message('required','%s harus diisi !');
				$this->form_validation->set_message('alpha_numeric_spaces', '%s Harus huruf / angka !');

				if($this->form_validation->run() == TRUE)
				{
					$this->load->model('m_kategori_obat');
					$kategori 	= $this->input->post('kategori');
					$insert 	= $this->m_kategori_obat->tambah_kategori($kategori);
					if($insert)
					{
						echo json_encode(array(
							'status' => 1,
							'pesan' => "<div class='alert alert-success'><i class='fa fa-check'></i> <b>".$kategori."</b> berhasil ditambahkan.</div>"
						));
					}
					else
					{
						$this->query_error();
					}
				}
				else
				{
					$this->input_error();
				}
			}
			else
			{
				$this->load->view('obat/kategori/kategori_tambah');
			}
		}
	}

	public function hapus_kategori($id_kategori_obat)
	{
		$level = $this->session->userdata('ap_level');
		if($level == 'admin' OR $level == 'inventory')
		{
			if($this->input->is_ajax_request())
			{
				$this->load->model('m_kategori_obat');
				$hapus = $this->m_kategori_obat->hapus_kategori($id_kategori_obat);
				if($hapus)
				{
					echo json_encode(array(
						"pesan" => "<font color='green'><i class='fa fa-check'></i> Data berhasil dihapus !</font>
					"));
				}
				else
				{
					echo json_encode(array(
						"pesan" => "<font color='red'><i class='fa fa-warning'></i> Terjadi kesalahan, coba lagi !</font>
					"));
				}
			}
		}
	}

	public function edit_kategori($id_kategori_obat = NULL)
	{
		if( ! empty($id_kategori_obat))
		{
			$level = $this->session->userdata('ap_level');
			if($level == 'admin' OR $level == 'inventory')
			{
				if($this->input->is_ajax_request())
				{
					$this->load->model('m_kategori_obat');
					
					if($_POST)
					{
						$this->load->library('form_validation');
						$this->form_validation->set_rules('kategori','Kategori','trim|required|max_length[40]|alpha_numeric_spaces');				
						$this->form_validation->set_message('required','%s harus diisi !');
						$this->form_validation->set_message('alpha_numeric_spaces', '%s Harus huruf / angka !');

						if($this->form_validation->run() == TRUE)
						{
							$kategori 	= $this->input->post('kategori');
							$insert 	= $this->m_kategori_obat->update_kategori($id_kategori_obat, $kategori);
							if($insert)
							{
								echo json_encode(array(
									'status' => 1,
									'pesan' => "<div class='alert alert-success'><i class='fa fa-check'></i> Data berhasil diupdate.</div>"
								));
							}
							else
							{
								$this->query_error();
							}
						}
						else
						{
							$this->input_error();
						}
					}
					else
					{
						$dt['kategori'] = $this->m_kategori_obat->get_baris($id_kategori_obat)->row();
						$this->load->view('obat/kategori/kategori_edit', $dt);
					}
				}
			}
		}
	}

	public function cek_stok()
	{
		if($this->input->is_ajax_request())
		{
			$this->load->model('m_obat');
			$kode = $this->input->post('kode_obat');
			$stok = $this->input->post('stok');

			$get_stok = $this->m_obat->get_stok($kode);
			if($stok > $get_stok->row()->total_stok)
			{
				echo json_encode(array('status' => 0, 'pesan' => "Stok untuk <b>".$get_stok->row()->nama_obat."</b> saat ini hanya tersisa <b>".$get_stok->row()->total_stok."</b> !"));
			}
			else
			{
				echo json_encode(array('status' => 1));
			}
		}
	}
}