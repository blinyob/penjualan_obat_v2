-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 11, 2019 at 04:44 PM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `penjualan`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `code` varchar(5) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`code`, `description`) VALUES
('c1', 'Category One'),
('c2', 'Category Two');

-- --------------------------------------------------------

--
-- Table structure for table `pj_akses`
--

CREATE TABLE `pj_akses` (
  `id_akses` tinyint(1) UNSIGNED NOT NULL,
  `label` varchar(10) NOT NULL,
  `level_akses` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_akses`
--

INSERT INTO `pj_akses` (`id_akses`, `label`, `level_akses`) VALUES
(1, 'admin', 'Administrator'),
(4, 'keuangan', 'Staff Keuangan');

-- --------------------------------------------------------

--
-- Table structure for table `pj_barang`
--

CREATE TABLE `pj_barang` (
  `id_barang` int(1) UNSIGNED NOT NULL,
  `kode_barang` varchar(40) NOT NULL,
  `nama_barang` varchar(60) NOT NULL,
  `total_stok` mediumint(1) UNSIGNED NOT NULL,
  `harga` decimal(10,0) NOT NULL,
  `id_kategori_obat` mediumint(1) UNSIGNED NOT NULL,
  `id_golongan_obat` mediumint(1) UNSIGNED DEFAULT NULL,
  `keterangan` text NOT NULL,
  `dihapus` enum('tidak','ya') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_barang`
--

INSERT INTO `pj_barang` (`id_barang`, `kode_barang`, `nama_barang`, `total_stok`, `harga`, `id_kategori_obat`, `id_golongan_obat`, `keterangan`, `dihapus`) VALUES
(1, '', 'Nike Sport C993', 4, '400000', 1, 2, '', 'tidak'),
(2, '', 'Runme Everynight Y98', 45, '120000', 3, 6, '', 'tidak'),
(3, '', 'My Lovely Bag 877', 29, '350000', 2, 3, '', 'tidak'),
(4, '', 'Quick Silver Gaul', 14, '35000', 3, 5, '', 'tidak'),
(5, '', 'My Cool Shoes', 39, '550000', 1, 2, '', 'ya'),
(6, '', 'Testing', 45, '929992', 1, 6, '', 'ya'),
(7, '', 'Tes ada', 67, '600000', 3, 3, '', 'ya'),
(8, '', 'Yes desk', 88, '999999', 1, 3, '', 'ya'),
(9, '', 'Test', 18, '100000', 3, 1, '', 'ya'),
(10, '', 'Test', 9, '99', 1, 2, '', 'ya'),
(11, '', 'Rinso', 17, '30000', 3, NULL, '', 'ya'),
(12, '', 'mouse', 20, '20000', 3, 1, '', 'ya'),
(13, '', 'Soklin Lantai', 20, '3000', 3, 1, '', 'ya'),
(14, '', 'Beras Merah', 15, '2000', 3, 1, '', 'ya'),
(16, '', 'Testing', 20, '3000', 4, 3, '', 'ya'),
(17, '', 'gatel', 12, '80001', 4, NULL, '', 'tidak'),
(18, '', 'dadaaa', 14, '80000', 9, NULL, '', 'tidak'),
(19, '', 'aku', 12, '80000', 9, NULL, '', 'ya'),
(20, '', 'aku', 12, '80000', 9, NULL, '', 'ya');

-- --------------------------------------------------------

--
-- Table structure for table `pj_ci_sessions`
--

CREATE TABLE `pj_ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_ci_sessions`
--

INSERT INTO `pj_ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('cbbo4bkeksl75gr7mvd3370f3beo0oc2', '::1', 1560258971, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536303235383937313b61705f69645f757365727c733a313a2231223b61705f70617373776f72647c733a34303a2264303333653232616533343861656235363630666332313430616563333538353063346461393937223b61705f6e616d617c733a32323a224d616d616e2053756b616e612c532e53692e2c417074223b61705f6c6576656c7c733a353a2261646d696e223b61705f6c6576656c5f63617074696f6e7c733a31333a2241646d696e6973747261746f72223b),
('i4oep9i6shpq26ncgnduptf3re5el70t', '::1', 1560259348, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536303235393334383b61705f69645f757365727c733a313a2231223b61705f70617373776f72647c733a34303a2264303333653232616533343861656235363630666332313430616563333538353063346461393937223b61705f6e616d617c733a32323a224d616d616e2053756b616e612c532e53692e2c417074223b61705f6c6576656c7c733a353a2261646d696e223b61705f6c6576656c5f63617074696f6e7c733a31333a2241646d696e6973747261746f72223b),
('0ltml8skundi9v9qa2fo6in2pad6tms3', '::1', 1560259835, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536303235393833353b61705f69645f757365727c733a313a2231223b61705f70617373776f72647c733a34303a2264303333653232616533343861656235363630666332313430616563333538353063346461393937223b61705f6e616d617c733a32323a224d616d616e2053756b616e612c532e53692e2c417074223b61705f6c6576656c7c733a353a2261646d696e223b61705f6c6576656c5f63617074696f6e7c733a31333a2241646d696e6973747261746f72223b),
('4qbtvq1di5duijtgb9a0fq38h38a4q07', '::1', 1560260249, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536303236303234393b61705f69645f757365727c733a313a2231223b61705f70617373776f72647c733a34303a2264303333653232616533343861656235363630666332313430616563333538353063346461393937223b61705f6e616d617c733a32323a224d616d616e2053756b616e612c532e53692e2c417074223b61705f6c6576656c7c733a353a2261646d696e223b61705f6c6576656c5f63617074696f6e7c733a31333a2241646d696e6973747261746f72223b),
('93ceaiqmpuf1jm0naeb93j35a2vhlae3', '::1', 1560260809, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536303236303830393b61705f69645f757365727c733a313a2231223b61705f70617373776f72647c733a34303a2264303333653232616533343861656235363630666332313430616563333538353063346461393937223b61705f6e616d617c733a32323a224d616d616e2053756b616e612c532e53692e2c417074223b61705f6c6576656c7c733a353a2261646d696e223b61705f6c6576656c5f63617074696f6e7c733a31333a2241646d696e6973747261746f72223b),
('uvudle58vsj55l9kgr7mqvcajgrql4r0', '::1', 1560261225, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536303236313232353b61705f69645f757365727c733a313a2231223b61705f70617373776f72647c733a34303a2264303333653232616533343861656235363630666332313430616563333538353063346461393937223b61705f6e616d617c733a32323a224d616d616e2053756b616e612c532e53692e2c417074223b61705f6c6576656c7c733a353a2261646d696e223b61705f6c6576656c5f63617074696f6e7c733a31333a2241646d696e6973747261746f72223b),
('iurfc1k4cl87a3gmp4n842b2p6g3cv3q', '::1', 1560261551, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536303236313535313b61705f69645f757365727c733a313a2231223b61705f70617373776f72647c733a34303a2264303333653232616533343861656235363630666332313430616563333538353063346461393937223b61705f6e616d617c733a32323a224d616d616e2053756b616e612c532e53692e2c417074223b61705f6c6576656c7c733a353a2261646d696e223b61705f6c6576656c5f63617074696f6e7c733a31333a2241646d696e6973747261746f72223b),
('0cf6pgsup99s65jj7sl4622b4iedqacr', '::1', 1560261982, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536303236313938323b61705f69645f757365727c733a313a2231223b61705f70617373776f72647c733a34303a2264303333653232616533343861656235363630666332313430616563333538353063346461393937223b61705f6e616d617c733a32323a224d616d616e2053756b616e612c532e53692e2c417074223b61705f6c6576656c7c733a353a2261646d696e223b61705f6c6576656c5f63617074696f6e7c733a31333a2241646d696e6973747261746f72223b),
('clsno0oci5clmtddbdip1i6c61b3kn00', '::1', 1560262330, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536303236323333303b61705f69645f757365727c733a313a2231223b61705f70617373776f72647c733a34303a2264303333653232616533343861656235363630666332313430616563333538353063346461393937223b61705f6e616d617c733a32323a224d616d616e2053756b616e612c532e53692e2c417074223b61705f6c6576656c7c733a353a2261646d696e223b61705f6c6576656c5f63617074696f6e7c733a31333a2241646d696e6973747261746f72223b),
('u80mnrn1bsmpb91dcsvhjur0nov4lnlr', '::1', 1560262925, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536303236323932353b61705f69645f757365727c733a313a2231223b61705f70617373776f72647c733a34303a2264303333653232616533343861656235363630666332313430616563333538353063346461393937223b61705f6e616d617c733a32323a224d616d616e2053756b616e612c532e53692e2c417074223b61705f6c6576656c7c733a353a2261646d696e223b61705f6c6576656c5f63617074696f6e7c733a31333a2241646d696e6973747261746f72223b),
('3v78qdpoavp0jdp4ijlbhbvd9k3aefug', '::1', 1560263251, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536303236333235313b61705f69645f757365727c733a313a2231223b61705f70617373776f72647c733a34303a2264303333653232616533343861656235363630666332313430616563333538353063346461393937223b61705f6e616d617c733a32323a224d616d616e2053756b616e612c532e53692e2c417074223b61705f6c6576656c7c733a353a2261646d696e223b61705f6c6576656c5f63617074696f6e7c733a31333a2241646d696e6973747261746f72223b),
('79l8dsd3gmj3upndqn4550khs6jotarj', '::1', 1560263364, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536303236333235313b61705f69645f757365727c733a313a2231223b61705f70617373776f72647c733a34303a2264303333653232616533343861656235363630666332313430616563333538353063346461393937223b61705f6e616d617c733a32323a224d616d616e2053756b616e612c532e53692e2c417074223b61705f6c6576656c7c733a353a2261646d696e223b61705f6c6576656c5f63617074696f6e7c733a31333a2241646d696e6973747261746f72223b);

-- --------------------------------------------------------

--
-- Table structure for table `pj_distributor`
--

CREATE TABLE `pj_distributor` (
  `id_distributor` mediumint(1) UNSIGNED NOT NULL,
  `nama` varchar(40) NOT NULL,
  `alamat` text,
  `telp` varchar(40) DEFAULT NULL,
  `info_tambahan` text,
  `kode_unik` varchar(30) NOT NULL,
  `waktu_input` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_distributor`
--

INSERT INTO `pj_distributor` (`id_distributor`, `nama`, `alamat`, `telp`, `info_tambahan`, `kode_unik`, `waktu_input`) VALUES
(1, 'aefefa', 'efaef', '3525235', '', '15599149791', '2019-06-07 20:42:59'),
(2, 'efeqf', 'eqfef', '341353', '', '15599150031', '2019-06-07 20:43:23');

-- --------------------------------------------------------

--
-- Table structure for table `pj_golongan_obat`
--

CREATE TABLE `pj_golongan_obat` (
  `id_golongan_obat` mediumint(1) UNSIGNED NOT NULL,
  `golongan` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_golongan_obat`
--

INSERT INTO `pj_golongan_obat` (`id_golongan_obat`, `golongan`) VALUES
(11, 'sdasdas');

-- --------------------------------------------------------

--
-- Table structure for table `pj_golongan_obatan`
--

CREATE TABLE `pj_golongan_obatan` (
  `id_golongan_obatan` mediumint(1) UNSIGNED NOT NULL,
  `golongan` varchar(40) NOT NULL,
  `dihapus` enum('tidak','ya') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_golongan_obatan`
--

INSERT INTO `pj_golongan_obatan` (`id_golongan_obatan`, `golongan`, `dihapus`) VALUES
(1, 'Sepatu', 'ya'),
(2, 'Tas', 'ya'),
(3, 'Baju', 'ya'),
(4, 'Celana', 'ya'),
(5, 'Topi', 'ya'),
(6, 'Gelang', 'ya'),
(7, 'Jam', 'ya'),
(8, 'Topi', 'ya'),
(9, 'Generik', 'tidak');

-- --------------------------------------------------------

--
-- Table structure for table `pj_kategori_barang`
--

CREATE TABLE `pj_kategori_barang` (
  `id_kategori_barang` mediumint(1) UNSIGNED NOT NULL,
  `kategori` varchar(40) NOT NULL,
  `dihapus` enum('tidak','ya') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_kategori_barang`
--

INSERT INTO `pj_kategori_barang` (`id_kategori_barang`, `kategori`, `dihapus`) VALUES
(1, 'Sepatu', 'ya'),
(2, 'Tas', 'ya'),
(3, 'Baju', 'ya'),
(4, 'Celana', 'ya'),
(5, 'Topi', 'ya'),
(6, 'Gelang', 'ya'),
(7, 'Jam', 'ya'),
(8, 'Topi', 'ya'),
(9, 'Generik', 'tidak');

-- --------------------------------------------------------

--
-- Table structure for table `pj_kategori_obat`
--

CREATE TABLE `pj_kategori_obat` (
  `id_kategori_obat` mediumint(1) UNSIGNED NOT NULL,
  `kategori` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_kategori_obat`
--

INSERT INTO `pj_kategori_obat` (`id_kategori_obat`, `kategori`) VALUES
(11, 'obat batuk');

-- --------------------------------------------------------

--
-- Table structure for table `pj_kategori_obatan`
--

CREATE TABLE `pj_kategori_obatan` (
  `id_kategori_obatan` mediumint(1) UNSIGNED NOT NULL,
  `kategori` varchar(40) NOT NULL,
  `dihapus` enum('tidak','ya') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_kategori_obatan`
--

INSERT INTO `pj_kategori_obatan` (`id_kategori_obatan`, `kategori`, `dihapus`) VALUES
(1, 'Sepatu', 'ya'),
(2, 'Tas', 'ya'),
(3, 'Baju', 'ya'),
(4, 'Celana', 'ya'),
(5, 'Topi', 'ya'),
(6, 'Gelang', 'ya'),
(7, 'Jam', 'ya'),
(8, 'Topi', 'ya'),
(9, 'Generik', 'tidak');

-- --------------------------------------------------------

--
-- Table structure for table `pj_merk_barang`
--

CREATE TABLE `pj_merk_barang` (
  `id_merk_barang` mediumint(1) UNSIGNED NOT NULL,
  `merk` varchar(40) NOT NULL,
  `dihapus` enum('tidak','ya') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_merk_barang`
--

INSERT INTO `pj_merk_barang` (`id_merk_barang`, `merk`, `dihapus`) VALUES
(1, 'Adidas', 'tidak'),
(2, 'Nike', 'tidak'),
(3, 'BodyPack', 'tidak'),
(4, 'Jansport', 'tidak'),
(5, 'Nevada', 'tidak'),
(6, 'Jackloth', 'tidak'),
(7, 'Pierro', 'ya'),
(8, 'Pierro', 'ya'),
(9, 'Pierro', 'ya'),
(10, 'Converse', 'tidak'),
(11, 'Piero', 'ya'),
(12, 'Teen', 'ya'),
(13, 'adass2', 'ya'),
(14, 'asda', 'ya'),
(15, 'sada3', 'ya'),
(16, 'asda 3', 'ya'),
(17, '333', 'ya');

-- --------------------------------------------------------

--
-- Table structure for table `pj_obat`
--

CREATE TABLE `pj_obat` (
  `kode_obat` int(1) UNSIGNED NOT NULL,
  `nama_obat` varchar(60) NOT NULL,
  `total_stok` mediumint(1) UNSIGNED NOT NULL,
  `harga` decimal(10,0) NOT NULL,
  `id_kategori_obat` mediumint(1) UNSIGNED NOT NULL,
  `id_golongan_obat` mediumint(1) UNSIGNED NOT NULL,
  `tgl_pembuatan` datetime NOT NULL,
  `tgl_kadaluarsa` date NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_obat`
--

INSERT INTO `pj_obat` (`kode_obat`, `nama_obat`, `total_stok`, `harga`, `id_kategori_obat`, `id_golongan_obat`, `tgl_pembuatan`, `tgl_kadaluarsa`, `keterangan`) VALUES
(42, 'sdsD', 12, '12000', 11, 11, '2019-06-11 22:28:19', '2019-06-10', '');

-- --------------------------------------------------------

--
-- Table structure for table `pj_obatan`
--

CREATE TABLE `pj_obatan` (
  `kode_obatan` int(1) UNSIGNED NOT NULL,
  `nama_obatan` varchar(60) NOT NULL,
  `total_stok` mediumint(1) UNSIGNED NOT NULL,
  `harga` decimal(10,0) NOT NULL,
  `id_kategori_obatan` mediumint(1) UNSIGNED NOT NULL,
  `id_golongan_obatan` mediumint(1) UNSIGNED NOT NULL,
  `tgl_pembuatan` datetime NOT NULL,
  `tgl_kadaluarsa` date NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_obatan`
--

INSERT INTO `pj_obatan` (`kode_obatan`, `nama_obatan`, `total_stok`, `harga`, `id_kategori_obatan`, `id_golongan_obatan`, `tgl_pembuatan`, `tgl_kadaluarsa`, `keterangan`) VALUES
(41, 'adsff', 32, '1200011', 9, 3, '2019-06-11 19:07:20', '2019-06-12', '');

-- --------------------------------------------------------

--
-- Table structure for table `pj_obatku`
--

CREATE TABLE `pj_obatku` (
  `kode_obat` int(1) UNSIGNED NOT NULL,
  `nama_obat` varchar(60) NOT NULL,
  `total_stok` mediumint(1) UNSIGNED NOT NULL,
  `harga` decimal(10,0) NOT NULL,
  `id_kategori_obat` mediumint(1) UNSIGNED NOT NULL,
  `id_golongan_obat` mediumint(1) UNSIGNED DEFAULT NULL,
  `id_satuan_obat` mediumint(1) UNSIGNED NOT NULL,
  `tgl_pembuatan` datetime NOT NULL,
  `tgl_kad` date NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_obatku`
--

INSERT INTO `pj_obatku` (`kode_obat`, `nama_obat`, `total_stok`, `harga`, `id_kategori_obat`, `id_golongan_obat`, `id_satuan_obat`, `tgl_pembuatan`, `tgl_kad`, `keterangan`) VALUES
(10, 'weqweqwewe', 21112, '112000', 11, 11, 11, '2019-06-07 12:28:37', '2019-06-18', 'wefaf'),
(11, 'weqweqwewqe', 10, '12000', 11, 11, 11, '2019-06-13 14:21:55', '2019-06-19', ''),
(12, 'wqeweqwe', 12, '12000', 11, 11, 11, '2019-06-12 10:41:37', '2019-06-06', ''),
(13, 'wqewewqe', 11, '12000', 11, 11, 11, '2019-06-12 10:41:37', '2019-06-06', ''),
(14, 'wefwerwer', 1, '12000', 11, 11, 11, '2019-06-12 10:53:12', '2019-06-10', '');

-- --------------------------------------------------------

--
-- Table structure for table `pj_pasien`
--

CREATE TABLE `pj_pasien` (
  `id_pasien` mediumint(1) UNSIGNED NOT NULL,
  `nama` varchar(11) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `info_tambahan` text NOT NULL,
  `kode_unik` varchar(40) NOT NULL,
  `waktu_input` datetime NOT NULL,
  `dihapus` enum('tidak','ya') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_pasien`
--

INSERT INTO `pj_pasien` (`id_pasien`, `nama`, `alamat`, `telp`, `info_tambahan`, `kode_unik`, `waktu_input`, `dihapus`) VALUES
(1, 'ssf', 'sfasf', '343544', '', '15597032641', '2019-06-05 09:54:24', 'tidak'),
(2, 'kasir', 'sdsdsdwewew', '35352', '', '15597033081', '2019-06-05 09:55:08', 'tidak'),
(3, 'efqe', 'erqr', '34134', '', '15597033511', '2019-06-05 09:55:51', 'tidak'),
(4, 'wetet', 'aeer', '34314', '', '15597033591', '2019-06-05 09:55:59', 'tidak'),
(5, 'kasir', 'werwer', '343141', '', '15597033651', '2019-06-05 09:56:05', 'tidak'),
(6, 'gsdg', 'dfgsdg', '32434', '', '15597033721', '2019-06-05 09:56:12', 'tidak'),
(7, 'sfsdgsd', 'segweg', '3235', '', '15597033781', '2019-06-05 09:56:18', 'tidak'),
(8, 'sgsdg', 'dsgsdg', '35332', '', '15597033841', '2019-06-05 09:56:24', 'tidak'),
(9, 'dgsdg', 'sdgdg', '3235235', '', '15597033901', '2019-06-05 09:56:30', 'tidak'),
(10, 'sdfgsdg', 'sdgfsdg', '23535', '', '15597033961', '2019-06-05 09:56:36', 'tidak'),
(11, 'dsgdg', 'sdgdg', '235325', '', '15597034031', '2019-06-05 09:56:43', 'tidak'),
(12, 'fgfjf', 'bhbhmb', '8709787', '', '15602245501', '2019-06-11 10:42:30', 'tidak'),
(13, 'dwdw', 'wdwd', '64626246', '', '15602439191', '2019-06-11 16:05:19', 'tidak');

-- --------------------------------------------------------

--
-- Table structure for table `pj_pelanggan`
--

CREATE TABLE `pj_pelanggan` (
  `id_pasien` mediumint(1) UNSIGNED NOT NULL,
  `nama` varchar(40) NOT NULL,
  `alamat` text,
  `telp` varchar(40) DEFAULT NULL,
  `info_tambahan` text,
  `kode_unik` varchar(30) NOT NULL,
  `waktu_input` datetime NOT NULL,
  `dihapus` enum('tidak','ya') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_pelanggan`
--

INSERT INTO `pj_pelanggan` (`id_pasien`, `nama`, `alamat`, `telp`, `info_tambahan`, `kode_unik`, `waktu_input`, `dihapus`) VALUES
(1, 'Pak Udin', 'Jalan Kayumanis 2 Baru', '08838493439', 'Testtt', '', '2016-05-07 22:44:25', 'ya'),
(2, 'Pak Jarwo', 'Kemanggisan deket binus', '4353535353', NULL, '', '2016-05-07 22:44:49', 'tidak'),
(3, 'Joko', 'Kayumanis', '08773682882', '', '', '2016-05-23 16:31:47', 'tidak'),
(4, 'Budi', 'Salemba', '089930393829', 'Testing', '', '2016-05-23 16:33:00', 'ya'),
(5, 'Mira', 'Pisangan', '09938829232', '', '', '2016-05-23 16:36:45', 'tidak'),
(6, 'Deden', 'Jauh', '990393', 'Test', '', '2016-05-24 20:54:58', 'ya'),
(7, 'Jamil', 'Berlan', '0934934939', '', '14640998941', '2016-05-24 21:24:54', 'tidak'),
(8, 'Budi', 'Jatinegara', '8349393439', '', '14640999321', '2016-05-24 21:25:32', 'tidak'),
(9, 'Kodok', 'Test', '0000', '', '14641003271', '2016-05-24 21:32:07', 'tidak'),
(10, 'Brandon', 'Test', '99030', '', '14641003401', '2016-05-24 21:32:20', 'tidak'),
(11, 'Broke', 'Test', '9900', 'Test', '14641005481', '2016-05-24 21:35:48', 'tidak'),
(12, 'Narji', 'Test', '000', 'Test', '14641006401', '2016-05-24 21:37:20', 'tidak'),
(13, 'Bernard', 'Test', '0000', 'test', '14641006651', '2016-05-24 21:37:45', 'tidak'),
(14, 'Nani', 'Test\r\n\r\nAja', '0000', 'Test\r\n\r\nAja', '14641016551', '2016-05-24 21:54:15', 'ya'),
(15, 'Norman', 'Test', '0039349', '', '14641017311', '2016-05-24 21:55:31', 'tidak'),
(16, 'Melina', 'Jauh', '9900039', 'Test', '14661682871', '2016-06-17 19:58:07', 'tidak'),
(17, 'Malih', 'test', '3434343', '', '14729767201', '2016-09-04 15:12:00', 'tidak'),
(18, 'jaka', 'jaka', '0000', 'jaka', '14729767881', '2016-09-04 15:13:08', 'tidak'),
(19, 'makak', 'kkk', '999', 'kakad', '14729768261', '2016-09-04 15:13:46', 'tidak'),
(20, 'asda', 'asda', '2342', 'asdad', '14729768371', '2016-09-04 15:13:57', 'tidak'),
(21, 'asdadadasdad', 'test', '324', 'asdadad', '14729768481', '2016-09-04 15:14:08', 'tidak'),
(22, 'jajang123', 'adadadwdwewe', '902942122222', '', '15594948791', '2019-06-03 00:01:19', 'tidak');

-- --------------------------------------------------------

--
-- Table structure for table `pj_pembelian_detail`
--

CREATE TABLE `pj_pembelian_detail` (
  `id_pembelian_d` int(1) UNSIGNED NOT NULL,
  `id_pembelian_m` int(1) UNSIGNED NOT NULL,
  `kode_obat` int(1) NOT NULL,
  `jumlah_beli` smallint(1) UNSIGNED NOT NULL,
  `harga_satuan` decimal(10,0) NOT NULL,
  `total` decimal(10,0) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_pembelian_detail`
--

INSERT INTO `pj_pembelian_detail` (`id_pembelian_d`, `id_pembelian_m`, `kode_obat`, `jumlah_beli`, `harga_satuan`, `total`) VALUES
(6, 33, 11, 1, '12000', '12000');

-- --------------------------------------------------------

--
-- Table structure for table `pj_pembelian_master`
--

CREATE TABLE `pj_pembelian_master` (
  `id_pembelian_m` int(1) UNSIGNED NOT NULL,
  `nomor_nota` varchar(40) NOT NULL,
  `tanggal` datetime NOT NULL,
  `grand_total` decimal(10,0) NOT NULL,
  `bayar` decimal(10,0) NOT NULL,
  `keterangan_lain` text,
  `id_distributor` mediumint(1) UNSIGNED DEFAULT NULL,
  `id_user` mediumint(1) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_pembelian_master`
--

INSERT INTO `pj_pembelian_master` (`id_pembelian_m`, `nomor_nota`, `tanggal`, `grand_total`, `bayar`, `keterangan_lain`, `id_distributor`, `id_user`) VALUES
(33, '5CFF878307CFB1', '2019-06-11 12:50:43', '12000', '50000', '', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pj_penjualan_detail`
--

CREATE TABLE `pj_penjualan_detail` (
  `id_penjualan_d` int(1) UNSIGNED NOT NULL,
  `id_penjualan_m` int(1) UNSIGNED NOT NULL,
  `kode_obat` int(1) NOT NULL,
  `jumlah_beli` smallint(1) UNSIGNED NOT NULL,
  `harga_satuan` decimal(10,0) NOT NULL,
  `total` decimal(10,0) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_penjualan_detail`
--

INSERT INTO `pj_penjualan_detail` (`id_penjualan_d`, `id_penjualan_m`, `kode_obat`, `jumlah_beli`, `harga_satuan`, `total`) VALUES
(1, 29, 18, 1, '80000', '80000'),
(2, 30, 3, 28, '350000', '9800000'),
(3, 31, 14, 11, '12000', '132000'),
(4, 32, 11, 1, '12000', '12000'),
(5, 32, 13, 1, '12000', '12000');

-- --------------------------------------------------------

--
-- Table structure for table `pj_penjualan_master`
--

CREATE TABLE `pj_penjualan_master` (
  `id_penjualan_m` int(1) UNSIGNED NOT NULL,
  `nomor_nota` varchar(40) NOT NULL,
  `tanggal` datetime NOT NULL,
  `grand_total` decimal(10,0) NOT NULL,
  `bayar` decimal(10,0) NOT NULL,
  `keterangan_lain` text,
  `id_pasien` mediumint(1) UNSIGNED DEFAULT NULL,
  `id_user` mediumint(1) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_penjualan_master`
--

INSERT INTO `pj_penjualan_master` (`id_penjualan_m`, `nomor_nota`, `tanggal`, `grand_total`, `bayar`, `keterangan_lain`, `id_pasien`, `id_user`) VALUES
(2, '57431A97D5DF8', '2016-05-23 16:58:31', '155000', '160000', '', 3, 1),
(3, '57431BDDAFA9D2', '2016-05-23 17:03:57', '350000', '400000', '', 3, 2),
(4, '57445D46655AB1', '2016-05-24 15:55:18', '250000', '260000', '', NULL, 1),
(6, '576406086CB611', '2016-06-17 16:15:36', '520000', '550000', '', NULL, 1),
(7, '57655546C37441', '2016-06-18 16:05:58', '35000', '40000', '', NULL, 1),
(8, '57655552ABF781', '2016-06-18 16:06:10', '350000', '400000', '', NULL, 1),
(9, '577A31BABCDC51', '2016-07-04 11:51:54', '905000', '910000', '', NULL, 1),
(10, '577A3327991DC1', '2016-07-04 11:57:59', '870000', '880000', 'Dibayar Langsung', NULL, 1),
(11, '577A3793C67CB1', '2016-07-04 12:16:51', '750000', '750000', '', NULL, 1),
(12, '57CA627F897FB1', '2016-09-03 07:41:19', '700000', '800000', '', NULL, 1),
(15, '57CBD697806F61', '2016-09-04 10:08:55', '400000', '500000', '', NULL, 1),
(16, '5CF3D7BC3AE0D1', '2019-06-02 16:05:48', '350000', '400000', '', 10, 1),
(17, '5CF40668590811', '2019-06-02 19:24:56', '350000', '400000', '', NULL, 1),
(18, '5CF406D53C9AC1', '2019-06-02 19:26:45', '350000', '500000', '', NULL, 1),
(19, '5CF407781660B1', '2019-06-02 19:29:28', '35000', '2333333', '', NULL, 1),
(20, '5CF4085A0F65A1', '2019-06-02 19:33:14', '35000', '3444444', '', NULL, 1),
(21, '5CF409E249F0E1', '2019-06-02 19:39:46', '35000', '70000', '', NULL, 1),
(22, '5CF40A48610141', '2019-06-02 19:41:28', '120000', '800000', '', NULL, 1),
(23, '5CF40AD1EF4521', '2019-06-02 19:43:45', '35000', '60000', '', NULL, 1),
(24, '5CF40B3FEF57E1', '2019-06-02 19:45:35', '35000', '80000', '', NULL, 1),
(25, '5CF7374D0A7911', '2019-06-05 05:30:21', '35000', '40000', '', 11, 1),
(26, '5CF7392E109B61', '2019-06-05 05:38:22', '35000', '90000', '', 11, 1),
(27, '5CF73960711AE1', '2019-06-05 05:39:12', '350000', '34343434', '', 11, 1),
(28, '5CF739FFA532D1', '2019-06-05 05:41:51', '80000', '446444', '', 9, 1),
(29, '5CF73A669FD011', '2019-06-05 05:43:34', '80000', '34343434', '', 11, 1),
(30, '5CF7815E865491', '2019-06-05 10:46:22', '9800000', '33333333', '', 11, 1),
(31, '5CFF6EC674DFD1', '2019-06-11 11:05:10', '132000', '200000', '', 13, 1),
(32, '5CFF6F56085811', '2019-06-11 11:07:34', '24000', '50000', '', 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pj_satuan_obat`
--

CREATE TABLE `pj_satuan_obat` (
  `id_satuan_obat` mediumint(1) UNSIGNED NOT NULL,
  `satuan` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_satuan_obat`
--

INSERT INTO `pj_satuan_obat` (`id_satuan_obat`, `satuan`) VALUES
(12, 'Bungkus'),
(11, 'Dus1'),
(10, 'Pack');

-- --------------------------------------------------------

--
-- Table structure for table `pj_user`
--

CREATE TABLE `pj_user` (
  `id_user` mediumint(1) UNSIGNED NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(60) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `id_akses` tinyint(1) UNSIGNED NOT NULL,
  `status` enum('Aktif','Non Aktif') NOT NULL,
  `dihapus` enum('tidak','ya') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pj_user`
--

INSERT INTO `pj_user` (`id_user`, `username`, `password`, `nama`, `id_akses`, `status`, `dihapus`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Maman Sukana,S.Si.,Apt', 1, 'Aktif', 'tidak'),
(2, 'kasir', '8691e4fc53b99da544ce86e22acba62d13352eff', 'Centini', 2, 'Aktif', 'ya'),
(3, 'kasir2', '08dfc5f04f9704943a423ea5732b98d3567cbd49', 'Kasir Dua', 2, 'Aktif', 'ya'),
(4, 'jaka', '2ec22095503fe843326e7c19dd2ab98716b63e4d', 'Jaka Sembung', 3, 'Aktif', 'ya'),
(5, 'jaka', '2ec22095503fe843326e7c19dd2ab98716b63e4d', 'Jaka Sembung', 3, 'Aktif', 'ya'),
(6, 'joko', '97c358728f7f947c9a279ba9be88308395c7cc3a', 'Joko Haji', 4, 'Aktif', 'ya'),
(7, 'amir', '1dd89e5367785ba89076cd264daac0464fdf0d7b', 'amir', 3, 'Aktif', 'ya'),
(8, 'rizki', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'rizki', 4, 'Aktif', 'ya'),
(9, 'zakaria', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'rizki', 4, 'Aktif', 'ya'),
(10, 'rizki', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'rizki', 4, 'Aktif', 'tidak');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `code` varchar(5) NOT NULL,
  `description` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  `category_code` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_product`
-- (See below for the actual view)
--
CREATE TABLE `v_product` (
`code` varchar(5)
,`description` varchar(100)
,`price` int(11)
,`category` varchar(100)
);

-- --------------------------------------------------------

--
-- Structure for view `v_product`
--
DROP TABLE IF EXISTS `v_product`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_product`  AS  select `p`.`code` AS `code`,`p`.`description` AS `description`,`p`.`price` AS `price`,`c`.`description` AS `category` from (`product` `p` join `category` `c` on((`p`.`category_code` = `c`.`code`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `pj_akses`
--
ALTER TABLE `pj_akses`
  ADD PRIMARY KEY (`id_akses`);

--
-- Indexes for table `pj_barang`
--
ALTER TABLE `pj_barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `pj_ci_sessions`
--
ALTER TABLE `pj_ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `pj_distributor`
--
ALTER TABLE `pj_distributor`
  ADD PRIMARY KEY (`id_distributor`);

--
-- Indexes for table `pj_golongan_obat`
--
ALTER TABLE `pj_golongan_obat`
  ADD PRIMARY KEY (`id_golongan_obat`);

--
-- Indexes for table `pj_golongan_obatan`
--
ALTER TABLE `pj_golongan_obatan`
  ADD PRIMARY KEY (`id_golongan_obatan`);

--
-- Indexes for table `pj_kategori_barang`
--
ALTER TABLE `pj_kategori_barang`
  ADD PRIMARY KEY (`id_kategori_barang`);

--
-- Indexes for table `pj_kategori_obat`
--
ALTER TABLE `pj_kategori_obat`
  ADD PRIMARY KEY (`id_kategori_obat`);

--
-- Indexes for table `pj_kategori_obatan`
--
ALTER TABLE `pj_kategori_obatan`
  ADD PRIMARY KEY (`id_kategori_obatan`);

--
-- Indexes for table `pj_merk_barang`
--
ALTER TABLE `pj_merk_barang`
  ADD PRIMARY KEY (`id_merk_barang`);

--
-- Indexes for table `pj_obat`
--
ALTER TABLE `pj_obat`
  ADD PRIMARY KEY (`kode_obat`);

--
-- Indexes for table `pj_obatan`
--
ALTER TABLE `pj_obatan`
  ADD PRIMARY KEY (`kode_obatan`);

--
-- Indexes for table `pj_obatku`
--
ALTER TABLE `pj_obatku`
  ADD PRIMARY KEY (`kode_obat`);

--
-- Indexes for table `pj_pasien`
--
ALTER TABLE `pj_pasien`
  ADD PRIMARY KEY (`id_pasien`);

--
-- Indexes for table `pj_pelanggan`
--
ALTER TABLE `pj_pelanggan`
  ADD PRIMARY KEY (`id_pasien`);

--
-- Indexes for table `pj_pembelian_detail`
--
ALTER TABLE `pj_pembelian_detail`
  ADD PRIMARY KEY (`id_pembelian_d`);

--
-- Indexes for table `pj_pembelian_master`
--
ALTER TABLE `pj_pembelian_master`
  ADD PRIMARY KEY (`id_pembelian_m`);

--
-- Indexes for table `pj_penjualan_detail`
--
ALTER TABLE `pj_penjualan_detail`
  ADD PRIMARY KEY (`id_penjualan_d`);

--
-- Indexes for table `pj_penjualan_master`
--
ALTER TABLE `pj_penjualan_master`
  ADD PRIMARY KEY (`id_penjualan_m`);

--
-- Indexes for table `pj_satuan_obat`
--
ALTER TABLE `pj_satuan_obat`
  ADD PRIMARY KEY (`id_satuan_obat`);

--
-- Indexes for table `pj_user`
--
ALTER TABLE `pj_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`code`),
  ADD KEY `category_code` (`category_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pj_akses`
--
ALTER TABLE `pj_akses`
  MODIFY `id_akses` tinyint(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pj_barang`
--
ALTER TABLE `pj_barang`
  MODIFY `id_barang` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `pj_distributor`
--
ALTER TABLE `pj_distributor`
  MODIFY `id_distributor` mediumint(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pj_golongan_obat`
--
ALTER TABLE `pj_golongan_obat`
  MODIFY `id_golongan_obat` mediumint(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pj_golongan_obatan`
--
ALTER TABLE `pj_golongan_obatan`
  MODIFY `id_golongan_obatan` mediumint(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pj_kategori_barang`
--
ALTER TABLE `pj_kategori_barang`
  MODIFY `id_kategori_barang` mediumint(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pj_kategori_obat`
--
ALTER TABLE `pj_kategori_obat`
  MODIFY `id_kategori_obat` mediumint(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pj_kategori_obatan`
--
ALTER TABLE `pj_kategori_obatan`
  MODIFY `id_kategori_obatan` mediumint(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pj_merk_barang`
--
ALTER TABLE `pj_merk_barang`
  MODIFY `id_merk_barang` mediumint(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `pj_obat`
--
ALTER TABLE `pj_obat`
  MODIFY `kode_obat` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `pj_obatan`
--
ALTER TABLE `pj_obatan`
  MODIFY `kode_obatan` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `pj_obatku`
--
ALTER TABLE `pj_obatku`
  MODIFY `kode_obat` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `pj_pasien`
--
ALTER TABLE `pj_pasien`
  MODIFY `id_pasien` mediumint(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `pj_pelanggan`
--
ALTER TABLE `pj_pelanggan`
  MODIFY `id_pasien` mediumint(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `pj_pembelian_detail`
--
ALTER TABLE `pj_pembelian_detail`
  MODIFY `id_pembelian_d` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pj_pembelian_master`
--
ALTER TABLE `pj_pembelian_master`
  MODIFY `id_pembelian_m` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `pj_penjualan_detail`
--
ALTER TABLE `pj_penjualan_detail`
  MODIFY `id_penjualan_d` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pj_penjualan_master`
--
ALTER TABLE `pj_penjualan_master`
  MODIFY `id_penjualan_m` int(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `pj_satuan_obat`
--
ALTER TABLE `pj_satuan_obat`
  MODIFY `id_satuan_obat` mediumint(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `pj_user`
--
ALTER TABLE `pj_user`
  MODIFY `id_user` mediumint(1) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`category_code`) REFERENCES `category` (`code`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
